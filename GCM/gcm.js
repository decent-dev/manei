inject.define("GCM.gcm", [
    "GCM.config.config",
    function (config) {
        var self = {};

        var gcm = require('node-gcm');

        var q = require('q');

        self.push = function (device_tokens, message) {

            return q.Promise(function (resolve, reject) {

                var messagePush = new gcm.Message();
                var sender = new gcm.Sender(config.senderKey);

                messagePush.addData('title', message.title || 'Manei');
                messagePush.addData('message', message.msg || 'Sem Mensaagem');
                messagePush.addData('sound', 'notification');
                messagePush.addData('data', message.data);                

                messagePush.collapseKey = 'collapseKey_' + (new Date()).getTime();
                messagePush.delayWhileIdle = true;
                messagePush.timeToLive = 3;

                sender.send(messagePush, device_tokens, 4, function (result) {
                    for (var i in device_tokens)
                        console.log('push sent to: ', device_tokens[i]);

                    resolve();
                });
            });
        };

        return self;
    }
]);
