inject.define("logApi.log", [
    function () {
        var self = {};

        var trataErroRouter = function (err) {
            if(typeof err == 'string') return err;
            if(err.message) return err.message;
        };

        var MOUNTH = {
          'Jan' : 1,
          'Feb' : 2,
          'Mar' : 3,
          'Apr' : 4,
          'May' : 5,
          'Jun' : 6,
          'Jul' : 7,
          'Aug' : 8,
          'Sep' : 9,
          'Oct' : 10,
          'Nov' : 11,
          'Dec' : 12
        };

        var getDate = function() {
          var regexTime = /^(\w{3}) (\w{3}) (\d{1,2}) (\d{2,4}) (\d{2}:\d{2}:\d{2}) (.*)$/;

          var time = new Date().toString().replace(regexTime, '$5');
          var mounth = MOUNTH[new Date().toString().replace(regexTime, '$2')];
          var day = new Date().toString().replace(regexTime, '$3');
          return day + '/' + mounth + ' ' + time;
        };

        var getStack = function (err) {
          var sa = err.stack.split('at ')[2];
          var regex = /.*\){0,1}.*\/(\w*)\/(\w*\.js):(\d*):(\d*)\){0,1}\n.*/;
          return sa.replace(regex, '$1/$2:$3:$4');
        };

        self.trace = function () {
            var message = [];
            message.push('[' + getDate() + ']');
            message.push('[TRACE]');
            message.push('[' + getStack(new Error()) + ']');
            for(var m in arguments) message.push(arguments[m]);
            console.trace(message.join(' '));
        };

        self.log = function () {
            var message = [];
            message.push('[' + getDate() + ']');
            message.push('[LOG]');
            message.push('[' + getStack(new Error()) + ']');
            for(var m in arguments) message.push(arguments[m]);
            console.log(message.join(' '));
        };

        self.info = function () {
            var message = [];
            message.push('[' + getDate() + ']');
            message.push('[INFO]');
            message.push('[' + getStack(new Error()) + ']');
            for(var m in arguments) message.push(arguments[m]);
            console.log(message.join(' '));
        };

        self.error = function () {
            var message = [];
            message.push('[' + getDate() + ']');
            message.push('[ERROR]');
            message.push('[' + getStack(new Error()) + ']');
            for(var m in arguments) message.push(trataErroRouter(arguments[m]));
            console.log(message.join(' '));
        };

        self.warn = function () {
            var message = [];
            message.push('[' + getDate() + ']');
            message.push('[WARN]');
            message.push('[' + getStack(new Error()) + ']');
            for(var m in arguments) message.push(arguments[m]);
            console.log(message.join(' '));
        };

        self.success = function () {
            var message = [];
            message.push('[' + getDate() + ']');
            message.push('[SUCCESS]');
            message.push('[' + getStack(new Error()) + ']');
            for(var m in arguments) message.push(arguments[m]);
            console.log(message.join(' '));
        };

        return self;
    }
]);
