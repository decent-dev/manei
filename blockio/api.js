inject.define("blockio.api", [
    "blockio.blockio",
    "blockio.config.config",
    "logApi.log",
    function (blockio, config, console) {
        var self = {};

        var q = require('q');

        self.get_new_address = function (label) {
            return q.Promise(function (resolve, reject, notify) {
                blockio.get_new_address({
                    label: label
                }, function (err, obj) {
                    if (err) {
                        console.trace(err);
                        reject(err);
                        return;
                    }
                    resolve(obj);
                });
            });
        };

        self.get_balance = function () {
            return q.Promise(function (resolve, reject, notify) {
                blockio.get_balance({},
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.get_my_addresses = function () {
            return q.Promise(function (resolve, reject, notify) {
                blockio.get_my_addresses({},
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.get_label_balance = function (label) {
            return q.Promise(function (resolve, reject, notify) {
                blockio.get_address_balance({
                        label: label
                    },
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.get_address_by_label = function (label) {
            return q.Promise(function (resolve, reject, notify) {
                blockio.get_address_by_label({
                        label: label
                    },
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.get_new_address_or_create = function (label) {
            return q.Promise(function (resolve, reject, notify) {
                self.get_address_by_label(label)
                    .then(function (wallet) {
                        resolve(wallet);
                    }).catch(function (err) {
                        console.trace(err);
                        self.get_new_address(label)
                            .then(function (wallet) {
                                resolve(wallet);
                            })
                            .catch(function (err) {
                                console.trace(err);
                                reject(err);
                            })
                    });
            });
        };

        self.withdraw_from_addresses = function (from_addresses, to_addresses, amounts) {
            return q.Promise(function (resolve, reject, notify) {

                if (typeof amounts != 'number') amounts = parseFloat(amounts);

                amounts = amounts.toFixed(8);

                blockio.withdraw_from_addresses({
                        amounts: amounts,
                        from_addresses: from_addresses,
                        to_addresses: to_addresses,
                        pin: config.SECRET_PIN
                    },
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.withdraw_from_labels_to_addresses = function (from_labels, to_addresses, amounts) {
            return q.Promise(function (resolve, reject, notify) {

                if (typeof amounts != 'number') amounts = parseFloat(amounts);

                amounts = amounts.toFixed(8);

                blockio.withdraw_from_labels({
                        amounts: amounts,
                        from_labels: from_labels,
                        to_addresses: to_addresses,
                        pin: config.SECRET_PIN
                    },
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.withdraw_from_labels_to_labels = function (from_labels, to_labels, amounts) {
            return q.Promise(function (resolve, reject, notify) {

                if (typeof amounts != 'number') amounts = parseFloat(amounts);

                amounts = amounts.toFixed(8);

                blockio.withdraw_from_labels({
                        amounts: amounts,
                        from_labels: from_labels,
                        to_labels: to_labels,
                        pin: config.SECRET_PIN
                    },
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.get_network_fee_estimate = function (to_addresses, amounts) {
            return q.Promise(function (resolve, reject, notify) {

                if (typeof amounts != 'number') amounts = parseFloat(amounts);

                amounts = amounts.toFixed(8);

                blockio.get_network_fee_estimate({
                        amounts: amounts,
                        to_addresses: to_addresses
                    },
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }

                        if (obj && obj.status == 'success') return resolve(obj.data);
                        console.trace('Erro na estimativa de taxa');
                        return reject('Erro na estimativa de taxa');
                    });
            });
        };

        self.is_green_address = function (addresses) {
            return q.Promise(function (resolve, reject, notify) {
                blockio.is_green_address({
                        addresses: addresses
                    },
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.is_green_transaction = function (transaction_ids) {
            return q.Promise(function (resolve, reject, notify) {
                blockio.is_green_transaction({
                        transaction_ids: transaction_ids
                    },
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            reject(err);
                            return;
                        }
                        resolve(obj);
                    });
            });
        };

        self.get_transactions_with_address = function (obj) {
            return q.Promise(function (resolve, reject, notify) {

                if (!obj.type) {
                    console.trace('Parametro type indefinido');
                    return reject('Parametro type indefinido');
                }

                if (!obj.addresses) {
                    console.trace('Parametro addresses indefinido');
                    return reject('Parametro addresses indefinido');
                }

                if (obj.type.toLowerCase() != 'sent' && obj.type.toLowerCase() != 'received') {
                    console.trace('Parametro type inválido. opções [sent|received]');
                    return reject('Parametro type inválido. opções [sent|received]');

                }

                blockio.get_transactions(obj,
                    function (err, obj) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }
                        if (!obj) {
                            var msgtrace = ['A API Blockio retornou "undefined" para get_transactions'].join(' ');
                            console.trace(msgtrace);
                            return reject(msgtrace);
                        }
                        resolve(obj);
                    });
            });
        };

        return self;
    }
]);
