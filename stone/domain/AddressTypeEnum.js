inject.define("stone.domain.AddressTypeEnum", [
    function () {
        var self = function () {};

        self.Billing = 'Endereço de cobrança';
        self.Shipping = 'Endereço de entrega';
        self.Comercial = 'Endereço comercial';
        self.Residential = 'Endereço residencial';

        return self;
    }
]);
