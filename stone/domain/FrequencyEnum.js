inject.define("stone.domain.FrequencyEnum", [
    function () {
        var self = function () {};

        self.Undefined = 'Indefinido';
        self.Weekly = 'Semanal';
        self.Monthly = 'Mensal';
        self.Yearly = 'Anual';
        self.Daily = 'Diário';

        return self;
    }
]);
