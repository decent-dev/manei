inject.define("stone.domain.CreditCardOperationEnum", [
    function () {
        var self = function () {};

        self.AuthOnly = 'Somente autorização';
        self.AuthAndCapture = 'Autorização com captura';
        self.AuthAndCaptureWithDelay = 'Autorização com captura com atraso';

        return self;
    }
]);
