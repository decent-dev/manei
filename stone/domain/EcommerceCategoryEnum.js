inject.define("stone.domain.EcommerceCategoryEnum", [
    function () {
        var self = function () {};

        self.B2B = 'Business to business';
        self.B2C = 'Business to Commerce';

        return self;
    }
]);
