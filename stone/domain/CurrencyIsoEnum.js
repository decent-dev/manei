inject.define("stone.domain.CurrencyIsoEnum", [
    function () {
        var self = function () {};

        self.BRL = 'Real';
        self.EUR = 'Euro';
        self.USD = 'Dólar';
        self.ARS = 'Peso argentino';
        self.BOB = 'Boliviano';
        self.CLP = 'Peso chileno';
        self.COP = 'Peso colombiano';
        self.UYU = 'Peso uruguaio';
        self.MXN = 'Peso mexicano';
        self.PYG = 'Paraguayan guaraní';

        return self;
    }
]);
