inject.define("stone.domain.CreditCardBrandEnum", [
    function () {
        var self = function () {};

        self.Visa = 'Visa';
        self.Mastercard = 'MasterCard';
        self.Hipercard = 'Hipercard';
        self.Amex = 'Amex';
        self.Diners = 'Diners';
        self.Elo = 'Elo';
        self.Aura = 'Aura';
        self.Discover = 'Discover';
        self.CasaShow = 'Casa Show';
        self.Havan = 'Havan';
        self.HugCard = 'HugCard';
        self.AndarAki = 'AndarAki';
        self.LearderCard = 'LeaderCard';

        return self;
    }
]);
