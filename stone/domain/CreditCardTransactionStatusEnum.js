inject.define("stone.domain.CreditCardTransactionStatusEnum", [
    function () {
        var self = function () {};

        self.AuthorizedPendingCapture = 'Autorizado e pendente de captura';
        self.NotAuthorized = 'Não autorizado';
        self.ChargebackPreview = 'Previsão de chargeback';
        self.RefundPreview = 'Previsão de estorno';
        self.DepositPreview = 'Previsão de depósito';
        self.Captured = 'Capturado';
        self.PartialCapture = 'Capturado parcialmente';
        self.Refunded = 'Estornado';
        self.Voided = 'Cancelado';
        self.Deposited = 'Depositado';
        self.OpenedPendingAuth = 'Aberto e pendente de autorização';
        self.Chargeback = 'Chargedback';
        self.PendingVoid = 'Pendente de captura';
        self.Invalid = 'Inválido';
        self.PartialAuthorize = 'Autorizado parcialmente';
        self.PartialRefunded = 'Estornado parcialmente';
        self.OverCapture = 'Capturado com valor abaixo';
        self.PartialVoid = 'Parcialmente capturado';
        self.PendingRefund = 'Pendente de estorno';
        self.UnScheduled = 'Não agendado';
        self.Created = 'Criado';
        self.PartialAuthorized = 'Autorizado parcialmente';
        self.NotFoundInAcquirer = 'Não localizado na adquirente';
        self.WithError = 'Com erro';

        return self;
    }
]);
