inject.define("stone.domain.EmailTypeEnum", [
    function () {
        var self = function () {};

        self.Comercial = 'Comercial';
        self.Personal = 'Pessoal';

        return self;
    }
]);
