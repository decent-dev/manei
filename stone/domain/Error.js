inject.define("stone.domain.Error", [
    function () {
        var self = function () {};

        self['0000'] = {
            message: 'Transação autorizada',
            orientation: '#',
            isRetry: '#'
        };
        self['0001'] = {
            message: 'Transação autoriada',
            orientation: 'Verifique a identidade antes de autorizar',
            isRetry: '#'
        };
        self['1000'] = {
            message: 'Transação não autorizada',
            orientation: '#',
            isRetry: '#'
        };
        self['1001'] = {
            message: 'Cartão vencido',
            orientation: '#',
            isRetry: '#'
        };
        self['1002'] = {
            message: 'Transação não permitida',
            orientation: '#',
            isRetry: '#'
        };
        self['1003'] = {
            message: 'Rejeitado emissor',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['1004'] = {
            message: 'Cartão com restrição',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['1005'] = {
            message: 'Transação não autorizada',
            orientation: '#',
            isRetry: '#'
        };
        self['1006'] = {
            message: 'Tentativas de senha excedidas',
            orientation: '#',
            isRetry: '#'
        };
        self['1007'] = {
            message: 'Rejeitado emissor',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['1008'] = {
            message: 'Rejeitado emissor',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['1009'] = {
            message: 'Transação não autorizada',
            orientation: '#',
            isRetry: '#'
        };
        self['1010'] = {
            message: 'Valor inválido',
            orientation: '#',
            isRetry: '#'
        };
        self['1011'] = {
            message: 'Cartão inválido',
            orientation: '#',
            isRetry: '#'
        };
        self['1013'] = {
            message: 'Transação não autorizada',
            orientation: '#',
            isRetry: '#'
        };
        self['1014'] = {
            message: 'Tipo de conta inválido',
            orientation: 'O tipo de conta selecionado não existe. Ex: uma transação de crédito com um cartão de débito.',
            isRetry: '#'
        };
        self['1016'] = {
            message: 'Saldo insuficiente',
            orientation: '#',
            isRetry: 'Sim'
        };
        self['1017'] = {
            message: 'Senha inválida',
            orientation: '#',
            isRetry: 'Sim'
        };
        self['1019'] = {
            message: 'Transação não permitida',
            orientation: '#',
            isRetry: '#'
        };
        self['1020'] = {
            message: 'Transação não permitida',
            orientation: '#',
            isRetry: '#'
        };
        self['1021'] = {
            message: 'Rejeitado emissor',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['1022'] = {
            message: 'Cartão com restrição',
            orientation: '#',
            isRetry: '#'
        };
        self['1023'] = {
            message: 'Rejeitado emissor',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['1024'] = {
            message: 'Transação não permitida',
            orientation: '#',
            isRetry: '#'
        };
        self['1025'] = {
            message: 'Cartão bloqueado',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['1042'] = {
            message: 'Tipo de conta inválido',
            orientation: 'O tipo de conta selecionado não existe. Ex: uma transação de crédito com um cartão de débito.',
            isRetry: '#'
        };
        self['1045'] = {
            message: 'Código de segurança inválido',
            orientation: '#',
            isRetry: 'Sim'
        };
        self['2000'] = {
            message: 'Cartão com restrição',
            orientation: '#',
            isRetry: '#'
        };
        self['2001'] = {
            message: 'Cartão vencido',
            orientation: '#',
            isRetry: '#'
        };
        self['2002'] = {
            message: 'Transação não permitida',
            orientation: '#',
            isRetry: '#'
        };
        self['2003'] = {
            message: 'Rejeitado emissor',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['2004'] = {
            message: 'Cartão com restrição',
            orientation: 'Oriente o portador a entrar em contato com o banco emissor do cartão.',
            isRetry: '#'
        };
        self['2005'] = {
            message: 'Transação não autorizada',
            orientation: '#',
            isRetry: '#'
        };
        self['2006'] = {
            message: 'Tentativas de senha excedidas',
            orientation: '#',
            isRetry: '#'
        };
        self['2007'] = {
            message: 'Cartão com restrição',
            orientation: '#',
            isRetry: '#'
        };
        self['2008'] = {
            message: 'Cartão com restrição',
            orientation: '#',
            isRetry: '#'
        };
        self['2009'] = {
            message: 'Cartão com restrição',
            orientation: '#',
            isRetry: '#'
        };
        self['9102'] = {
            message: 'Transação inválida',
            orientation: '#',
            isRetry: '#'
        };
        self['9108'] = {
            message: 'Erro no processamento',
            orientation: '#',
            isRetry: 'Sim'
        };
        self['9109'] = {
            message: 'Erro no processamento',
            orientation: '#',
            isRetry: 'Sim'
        };
        self['9111'] = {
            message: 'Time-out na transação',
            orientation: '#',
            isRetry: 'Sim'
        };
        self['9112'] = {
            message: 'Emissor indisponível',
            orientation: '#',
            isRetry: 'Sim'
        };
        self['9999'] = {
            message: 'Erro não especificado',
            orientation: '#',
            isRetry: '#'
        };

        return self;
    }
]);
