inject.define("stone.domain.SeverityCodeEnum", [
    function () {
        var self = function () {};

        self.Error = 'Erro';
        self.Warning = 'Aviso';

        return self;
    }
]);
