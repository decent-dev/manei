inject.define("stone.domain.DocumentTypeEnum", [
    function () {
        var self = function () {};

        self.CPF = 'Cadastro de pessoa física';
        self.CNPJ = 'Cadastro de pessoa jurídica';

        return self;
    }
]);
