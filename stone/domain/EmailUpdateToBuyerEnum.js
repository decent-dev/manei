inject.define("stone.domain.EmailUpdateToBuyerEnum", [
    function () {
        var self = function () {};

        self.Yes = 'Sim';
        self.No = 'Não';
        self.YesIfAuthorized = 'Sim se autorizado';
        self.YesIfNotAuthorized = 'Sim se não autorizado';

        return self;
    }
]);
