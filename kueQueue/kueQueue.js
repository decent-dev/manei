inject.define("kueQueue.kueQueue", [
    "kueQueue.config.config",
    function (config) {
        var self = {};

        var express = require('express');
        var app = express();

        var kue = require('kue');
        var ui = require('kue-ui');

        var queue = kue.createQueue({
            prefix: 'q',
            redis: {
                port: config.port,
                host: config.host,
                auth: config.password,
                db: config.db, // if provided select a non-default redis db
                options: {
                    // see https://github.com/mranney/node_redis#rediscreateclient
                }
            }
        });

        ui.setup({
            apiURL: '/api', // IMPORTANT: specify the api url
            baseURL: '/kue', // IMPORTANT: specify the base url
            updateInterval: 5000 // Optional: Fetches new data every 5000 ms
        });

        // Mount kue JSON api
        app.use('/api', kue.app);
        // Mount UI
        app.use('/kue', ui.app);

        app.listen(config.portUI);

        return {
            create: function (_queue, obj) {
                return queue.create(_queue, obj)
                    .removeOnComplete(false)
                    .priority('high')
                    .attempts(5)
            },
            consumer : queue
        };
    }
]);
