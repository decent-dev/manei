inject.define("kueQueue.utils.util", [
    "logApi.log",
    function (console) {
        var self = {};

        self.logQueue = function (job) {
            job.on('complete', function (result) {
                console.success('Job completed with data ', result);
            }).on('failed attempt', function (traceMessage, doneAttempts) {
                console.error('Job failed', traceMessage);
            }).on('failed', function (traceMessage) {
                console.error('Job failed', traceMessage);
            }).on('progress', function (progress, data) {
                console.log('\r  job #' + job.id + ' ' + progress + '% complete with data ', data);
            });
        };        

        return self;
    }
]);
