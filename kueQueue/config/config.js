inject.define("kueQueue.config.config", [
    function () {
        var self = {};

        self.port = process.env.PORTREDIS || 6379;
        self.host = process.env.HOSTREDIS || 'localhost';
        self.password = undefined;
        self.db = 3;
        self.portUI = process.env.PORTKUE || 3001;

        return self;
    }
]);
