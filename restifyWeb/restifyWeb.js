inject.define("restifyWeb.restifyWeb", [
    "restifyWeb.config.config",
    "logApi.log",
    function (config, console) {

        var self = {};

        var restify = require('restify');
        var fs = require('fs');

        // Setup some https server options
        var https_options = {
            name: config.appName,
            version: config.version,
           // key: fs.readFileSync(__dirname + '/config/pem/server-key.pem'),
           // certificate: fs.readFileSync(__dirname + '/config/pem/server-crt.pem')
        };

        var server = restify.createServer(https_options);

        server.use(restify.acceptParser(server.acceptable));
        server.use(restify.queryParser());
        server.use(restify.bodyParser());
        server.use(restify.CORS());

        server.listen(config.appPort);

        console.info('PORT', config.appPort);

        return server;
    }
]);
