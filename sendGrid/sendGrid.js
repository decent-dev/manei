inject.define("sendGrid.sendGrid", [
    "sendGrid.config.config",
    "logApi.log",
    function (config, console) {

        var sendgrid = require("sendgrid")(config.api_user_proxy, config.api_key_proxy);

        console.log('Config sendGrid', config.api_user_proxy, config.api_key_proxy);

        return sendgrid;
    }
]);
