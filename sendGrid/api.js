var q = require('q');

inject.define("sendGrid.api", [
    "sendGrid.sendGrid",
    "sendGrid.config.config",
    "logApi.log",
    function (sendGrid, config, console) {
        var self = {};

        self.sendEmail = function (to, subject, body) {
            return q.Promise(function (resolve, reject, notify) {

                if (!to) {
                    var msgtrace = ['Não foi informado o destino da mensagem'].join(' ');
                    console.trace(msgtrace);
                    return reject(msgtrace);
                }

                if (!subject) {
                    var msgtrace = ['Mensagem sem Assunto'].join(' ');
                    console.trace(msgtrace);
                    return reject(msgtrace);
                }

                if (!body) {
                    var msgtrace = ['Mensagem sem conteúdo'].join(' ');
                    console.trace(msgtrace);
                    return reject(msgtrace);
                }

                var email = new sendGrid.Email({
                    fromname: config.fromname
                });

                email.addTo(to);

                email.setFrom(config.from);

                email.setSubject(subject);

                email.setText(body);

                sendGrid.send(email, function (err, json) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    resolve(json);
                });
            });
        };

        return self;
    }
]);
