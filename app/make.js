#!/usr/bin/env node

var program = require('commander');

var fs = require('fs');

var swig = require('swig');

var templates = [
    'www/index.html',
    'config.xml',
    'package.json',
    'ionic.project',
    'www/js/service/config/services-config.js'
];

var target = 'client',
        env = 'prod',
        domain = 'app.manei.com.br:443',
        dev = undefined;

program
        .version('0.0.1')
        .option('-t, --target [store/client] ', 'Estabelecimento ou Cliente, Default: client', function (_target) {
            target = _target;
        })
        .option('-e, --env [prod/dev] ', 'Ambiente, Default: prod', function (_env) {
            env = _env;
            dev = (_env == 'dev' ? _env : undefined);
        })
        .option('-d, --dominio [d] ', 'Dominio ex. app.manei.com.br, Default: app.manei.com.br', function (_domain) {
            domain = _domain;
        })

        .parse(process.argv);

console.log('Dados:');
console.log('\t', 'Target:', target);
console.log('\t', 'Env:', env);
console.log('\t', 'Domain:', domain);
console.log();
console.log('Ou -h para ver mais opções');

var template = function (__target, __env, __domain) {
    for (var i in templates) {

        var template = templates[i];

        var result = swig.renderFile(template + 't', {
            TARGET: __target,
            ENV: __env,
            DOMAIN: __domain
        });

        fs.writeFileSync(template, result);
    }
};


if (!dev) {
    console.log('Mode Prodution');
    template(target, env, domain);
} else {
    console.log('Mode Developer');    
    template('client', env, domain);
    fs.renameSync('www/index.html', 'www/client.html');
    template('store', env, domain);
    fs.renameSync('www/index.html', 'www/store.html');
}
