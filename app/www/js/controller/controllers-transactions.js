var app = angular.module('manei');

app.controller('TransactionsCtrl', function ($scope, $stateParams, $state, Http, Authentication, ShowMessage, Transaction) {

    var user = new Authentication().getUser();

    var transaction = new Transaction();

    $scope.load = function () {
        transaction.resolveNextPage(10, '-date')
            .then(function (list) {
                $scope.orders = list;
            })
            .catch(function (err) {
                return ShowMessage.showMessage(err, function () {});
            })
    };

    $scope.load();
    
});
