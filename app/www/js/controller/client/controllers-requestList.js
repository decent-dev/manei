var app = angular.module('manei');

app.controller('RequestListCtrl', function ($scope, $stateParams, $state, Http, Authentication, TimeAgo, Order, ShowMessage, NotificationAlert) {

    var user = new Authentication().getUser();

    $scope.NotificationAlert = NotificationAlert;

    $scope.reject = function (order) {
        order.processing = 'Cancelando o pedido';
        order = new Order().create(order);
        order.reject();
    };

    $scope.load = function () {
        var orderFactory = new Order();
        orderFactory.resolveNextPageByPersonFrom(10, '-dtcreate')
                .then(function (list) {
                    $scope.orders = list;

                    $scope.ordersSum = {};

                    for (var i in $scope.orders) {
                        var order = $scope.orders[i];

                        if (!$scope.ordersSum[TimeAgo.ago(order.dtcreate)])
                            $scope.ordersSum[TimeAgo.ago(order.dtcreate)] = [];

                        $scope.ordersSum[TimeAgo.ago(order.dtcreate)].push(order);
                    }
                })
                .finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                })
                .catch(function (err) {
                    return ShowMessage.showMessage(err, function () {});
                });
    };

    $scope.load();

    $scope.notification = function () {
        NotificationAlert.qtdeNotification = 0;
        $scope.load();
    };

});
