var app = angular.module('manei');

app.controller('RequestOrderCtrl', function ($scope, $stateParams, $state, Http, Authentication, Queue, Order, ShowMessage) {

    var user = new Authentication().getUser();

    console.log('RequestOrderCtrl');

    $scope.id = $stateParams.id;

    $scope.load = function () {
        $scope.order = new Order();
        $scope.order.load($scope.id, 'IDHash');
        $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.load();

});
