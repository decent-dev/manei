var app = angular.module('manei');

app.controller('BalanceCtrl', function ($scope, Authentication) {

    var person = new Authentication().getUser();

    $scope.load = function () {
        person.getBalanceLimite().then(function (limit) {
            $scope.balanceLimite = limit;
            person.balanceLimite = limit;
            person.saveCache();
            $scope.$broadcast('scroll.refreshComplete');
        });

        person.getBalanceSaldo().then(function (limit) {
            $scope.balanceSaldo = limit;
            person.balanceSaldo = limit;
            person.saveCache();
            $scope.$broadcast('scroll.refreshComplete');
        });
        
         
    };
    
    $scope.load();



});
