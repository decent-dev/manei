var app = angular.module('manei');

app.controller('InitWorkFlowCtrl', function ($q, $state, $scope, ShowMessage, ProtocolProduct, Order, QRCodeScan, Workflow, Authentication) {

    var protocolProduct = new ProtocolProduct();

    $scope.order = new Order();

    $scope.order.get();

    var person = new Authentication().getUser();
    
    var obs;

    //Assumindo que as vendas são Pos-pago/Crédito
    $scope.balanceLimite = person.balanceLimite;

    var workflow = new Workflow();

    $scope.remove = function (index, order) {
        try {
            $scope.order.removeItem(index);
            $scope.order.save();
        } catch (e) {
            ShowMessage.showMessage(e, function () {});
        }
    };

    var requestTable = function (order) {
        return $q(function (resolve, reject) {
            //TODO: TEMPORARIO PARA RESOLVER COM PATTERN COMMAND
            if (workflow.code != 1)
                return resolve(order);

            ShowMessage.showInput("Digite o numero da mesa", function (table) {

                if (!table) {
                    return reject('A mesa não foi informada');
                }

                for (var i in order.itens) {
                    order.itens[i].table = table;
                }

                return resolve(order);
            });
        });
    };

    var requestObs = function (order) {
        return $q(function (resolve, reject) {
            //TODO: TEMPORARIO PARA RESOLVER COM PATTERN COMMAND
            if (workflow.code != 1)
                return resolve(order);            

            ShowMessage.showInputText("Observação do Pedido", obs, function (_obs) {
                order.obs = _obs;
                obs = _obs;
                return resolve(order);
            });
        });
    };

    var requestAccountID = function (order) {
        return $q(function (resolve, reject) {
            //TODO: TEMPORARIO PARA RESOLVER COM PATTERN COMMAND
            if (workflow.code != 1)
                return resolve(order);

            ShowMessage.showInput("Digite o número da comanda", function (accountID) {

                if (!accountID) {
                    return reject('A comanda não foi informada');
                }

                for (var i in order.itens) {
                    order.itens[i].accountID = accountID;
                }

                return resolve(order);
            });
        });
    };

    $scope.payment = function (order) {

        requestObs(order)
                .then(requestTable)
                .then(requestAccountID)
                .then(function () {
                    return order.pay();
                })
                .then(function () {
                    order.save();
                    $state.go(workflow.nextStep());
                })
                .catch(function (err) {
                    return ShowMessage.showMessage(err, function () {});
                });

    };

    $scope.cancel = function () {
        $scope.order = new Order();
        $scope.order.save();
    };

    $scope.add = function (item) {
        try {
            $scope.order.addItemAndSort(item)
                    .then(function () {
                        $scope.order.save();
                    }).catch(function (err) {
                ShowMessage.showMessage(err, function () {});
            });
        } catch (e) {
            ShowMessage.showMessage(e, function () {});
        }
    };

    $scope.scan = function () {
        var qRCodeScan = new QRCodeScan();

        qRCodeScan.scan()
                .then(function (text) {
                    if (!text || text == null)
                        return;
                    var o = protocolProduct.decode(text);

                    var w = workflow.load(o.workflow);
                    if (!w) {
                        return ShowMessage.showMessage(['O WorkFlow', o.workflow, 'não existe'].join(' '), function () {});
                    }

                    //PRECISO SALVAR O ESTADO DO WORKFLOW LOGO APÓS ESCANEAR
                    workflow.save();

                    if (!o) {
                        return ShowMessage.showMessage("O QRCODE fora do padrão", function () {});

                    }

                    $scope.add(o);
                }).catch(function (err) {
            ShowMessage.showMessage("Scanning failed: " + err, function () {});
        });
    };

});
