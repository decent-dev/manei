var app = angular.module('manei');

app.controller('ProfileCtrl', function ($scope, ShowMessage, $state, Mobile, Person) {

    $scope.mobile = new Mobile();
    $scope.mobile.loadCache();

    $scope.save = function (_person) {

      var person = new Person();
      person.loadCache();
      person.create(_person);

        if (!person) {
            return ShowMessage.showMessage('Preencha o formulario', function () {});

        }

        if (!person.name) {
            return ShowMessage.showMessage('Preencha o nome', function () {});
        }

        if (!person.cpfCnpj) {
            return ShowMessage.showMessage('Preencha o CPF', function () {});
        }

        if (!person.password) {
            return ShowMessage.showMessage('Preencha o PIN', function () {});
        }

        if (person.password.length != 4) {
            return ShowMessage.showMessage('O PIN deve ter exatamente 4 dígitos', function () {});
        }

        if (person.password != person.cpassword) {
            return ShowMessage.showMessage('As senhas PIN\'s devem ser iguais', function () {});
        }

        $scope.mobile.person = person;

        $scope.mobile.save()
            .then(function (data) {

                delete person.cpassword;

                person.id = data.data.idUser;

                $scope.mobile.auth = data.data.authenticationCode;

                person.saveCache();

                $scope.mobile.saveCache();

                ShowMessage.showMessage(data.data.message, function () {
                    return $state.go('app.home');
                });
            }).catch(function (err) {
                return ShowMessage.showMessage(err, function () {});
            });

    };
});
