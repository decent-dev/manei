var app = angular.module('manei');

app.controller('PhoneNumberCtrl', function ($scope, ManeiConfig, ShowMessage, $state, Mobile) {

    $scope.mobile = new Mobile();
    $scope.mobile.loadCache();

    delete $scope.mobile.cphoneNumber;

    if (ManeiConfig.env == ManeiConfig.ENV.PRODUTION) {
        try {
            window.plugins.uniqueDeviceID.get(function (imei) {
                $scope.mobile.IMEI = imei;
                $scope.mobile.saveCache();
                console.log('Imersão com device IMEI:', imei);
            }, function () {
                alert("Erro ao ler o IMEI");
            });
        } catch (e) {
            alert(e);
        }
    } else {
        var imei = $scope.mobile.IMEI || ~~(Math.random() * 99999999999999);
        console.log('Imersão com Web IMEI:', imei);
        $scope.mobile.IMEI = imei;
    }

    $scope.send = function (imersao) {
        if ($scope.mobile.cphoneNumber != $scope.mobile.phoneNumber) {
            ShowMessage.showMessage("Os números informados devem ser iguais. Tente novamente",
                    function () {});
            return;
        }

        $scope.mobile.saveCache();

        $scope.mobile.imersao()
                .then(function () {
                    return $state.go('immersion.codesms');
                }).catch(function (err) {
            return ShowMessage.showMessage(err, function () {});
        });
    };
});
