var app = angular.module('manei');

app.controller('RequestListCtrl', function ($scope, $stateParams, $state, Http, Authentication, TimeAgo, Order, ShowMessage, Product, NotificationAlert) {

    var user = new Authentication().getUser();
    
    $scope.NotificationAlert = NotificationAlert;

    var productFactory = new Product();

    $scope.accept = function (order) {
        order.processing = 'Processando seu aceite';
        order = new Order().create(order);
        productFactory.attachAmount(order.itens)
                .then(function (o) {
                    order.accept();
                });
    };

    $scope.done = function (order) {
        order.processing = 'Processando o término';
        order = new Order().create(order);
        productFactory.attachAmount(order.itens)
                .then(function (o) {
                    order.done();
                });
    };

    $scope.load = function () {
        var orderFactory = new Order();
        orderFactory.resolveNextPageByPersonTo(10, '-dtcreate')
                .then(function (list) {
                    $scope.orders = list;

                    $scope.ordersSum = {};

                    for (var i in $scope.orders) {
                        var order = $scope.orders[i];

                        if (!$scope.ordersSum[TimeAgo.ago(order.dtcreate)])
                            $scope.ordersSum[TimeAgo.ago(order.dtcreate)] = [];

                        $scope.ordersSum[TimeAgo.ago(order.dtcreate)].push(order);
                    }
                })
                .finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                })
                .catch(function (err) {
                    return ShowMessage.showMessage(err, function () {});
                });
    };
    
    $scope.notification = function (){
        NotificationAlert.qtdeNotification = 0;
        $scope.load();
    };

    $scope.load();

});
