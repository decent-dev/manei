var app = angular.module('manei');

app.controller('RequestListGroupByAccountCtrl', function ($scope, $stateParams, $state, Http, Authentication, TimeAgo, Order, ShowMessage, Product, NotificationAlert) {

    var user = new Authentication().getUser();

    var productFactory = new Product();    

    $scope.load = function () {
        var orderFactory = new Order();
        orderFactory.resolveNextPageByPersonTo(100, 'dtcreate')
                .then(function (list) {
                    $scope.orders = list;

                    $scope.ordersSum = {};
                    
                    //pega os registro só de hoje
                    $scope.orders = $scope.orders.filter(function (o){
                        return (new Date(o.dtcreate).getDate()) == (new Date().getDate());
                    });

                    for (var i in $scope.orders) {

                        var order = $scope.orders[i];

                        //somente pedidos com status 3 (pronto para retirada)
                        if(order.status[order.status.length -1].code != 3) continue;

                        var accountID = parseInt(order.itens[0].accountID)

                        if (!$scope.ordersSum[accountID])
                            $scope.ordersSum[accountID] = [];

                        $scope.ordersSum[accountID].push(order);
                    }
                })
                .finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                })
                .catch(function (err) {
                    return ShowMessage.showMessage(err, function () {});
                });
    };

    $scope.load();

});
