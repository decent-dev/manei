var app = angular.module('manei');

app.controller('AppCtrl', function ($scope, ShowMessage, $state, Queue, AgentService, Person, GCMService) {

    new Queue('sync/orderAuthorization').service();

    $scope.sairShow = function () {
        ShowMessage.showMessage("Deseja desabilitar esse celular ?", function () {
            var person = new Person();
            person.loadCache();
            person.removeCache();
            $state.go('immersion.phonenumber');
        }, function () {

        });
    };
});
