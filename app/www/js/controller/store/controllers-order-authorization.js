var app = angular.module('manei');

app.controller('OrderAuthorizationCtrl', function ($scope, $stateParams, $state, ShowMessage, ManeiConfig, ProtocolOrder, Authentication, Order, Storage, ZIP, QRCodeScan, Product) {

    var user = new Authentication().getUser();

    var protocolOrder = new ProtocolOrder();

    var user = new Authentication().getUser();

    var productFactory = new Product();

    $scope.order = new Order();
    $scope.order.save();

    $scope.accept = function (order) {
        order = new Order().create(order);
        productFactory.attachAmount(order.itens)
            .then(function (o) {
                order.accept();
                $scope.order = new Order();
                $scope.order.save();
            });
    };

    $scope.cancel = function () {
        $scope.order = new Order();
        $scope.order.save();
    };

    $scope.scan = function () {

        var qRCodeScan = new QRCodeScan();

        qRCodeScan.scan().then(function (text) {
            var o = protocolOrder.decode(text);

            if (!o) {
                return ShowMessage.showMessage("O QRCODE fora do padrão", function () {});
            }

            if (user.id != o.to) {
                return ShowMessage.showMessage('O pedido não está destinado ao seu Estabelecimento', function () {});
            }

            productFactory.attachAmount(o.itens)
                .then(function (_o) {
                  $scope.order = o;
                });

        }).catch(function (err) {
            ShowMessage.showMessage("Scanning failed: " + err, function () {});
        });
    };

});
