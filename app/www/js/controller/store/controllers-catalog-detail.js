var app = angular.module('manei');

app.controller('CatalogDetailCtrl', function ($scope, $stateParams, $state, ProtocolProduct, Storage, Product) {

    var protocolProduct = new ProtocolProduct();  

    var get = function (id) {
        var productFactory = new Product();
        productFactory.resolveLoad(id)
            .then(function (domain) {
                $scope.catalog = domain;
                $scope.catalog.qr = protocolProduct.encode(domain);
            });
    };

    get($stateParams.id);

});
