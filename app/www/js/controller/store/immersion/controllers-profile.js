var app = angular.module('manei');

app.controller('ProfileCtrl', function ($scope, ShowMessage, $state, Mobile, Person) {

    $scope.mobile = new Mobile();
    $scope.mobile.loadCache();

    $scope.next = function (_person) {

        var person = new Person();
        person.create(_person);

        if (!person.cpfCnpj) {
            return ShowMessage.showMessage('Preencha o CNPJ', function () {});
        }

        person.saveCache();

        person.existsByCpfCnpj()
            .then(function (res) {
                if (res.data.exists) {
                    $scope.mobile.cpfCnpj = person.cpfCnpj;
                    $scope.mobile.imersaoSeason()
                        .then(function () {
                            $state.go('immersion.profile-step-3');
                        });
                } else {
                    $state.go('immersion.profile-step-2');
                }
            })

    };

    $scope.saveSeason = function (_mobile) {

        var person = new Person();
        person.loadCache();

        var mobile = new Mobile();
        mobile.loadCache();
        mobile.create(_mobile);

        if (!mobile.seasonName) {
            return ShowMessage.showMessage('Preencha o nome da Estação', function () {});
        }

        $scope.mobile.person = person;

        $scope.mobile.saveSeason()
            .then(function (data) {

                delete person.cpassword;

                person.id = data.data.idUser;

                $scope.mobile.auth = data.data.authenticationCode;

                person.saveCache();

                $scope.mobile.saveCache();

                ShowMessage.showMessage(data.data.message, function () {
                    $state.go('app.home');
                });
            }).catch(function (err) {
                return ShowMessage.showMessage(err, function () {});
            });
    };

    $scope.save = function (_person) {

        var person = new Person();
        person.loadCache();
        person.create(_person);

        if (!person) {
            return ShowMessage.showMessage('Preencha o formulario', function () {});

        }

        if (!person.name) {
            return ShowMessage.showMessage('Preencha o nome', function () {});
        }

        if (!person.password) {
            return ShowMessage.showMessage('Preencha o PIN', function () {});
        }

        if (person.password.length != 4) {
            return ShowMessage.showMessage('O PIN deve ter exatamente 4 dígitos', function () {});
        }

        if (person.password != person.cpassword) {
            return ShowMessage.showMessage('As senhas PIN\'s devem ser iguais', function () {});
        }

        $scope.mobile.person = person;

        $scope.mobile.save()
            .then(function (data) {

                delete person.cpassword;

                person.id = data.data.idUser;

                $scope.mobile.auth = data.data.authenticationCode;

                person.saveCache();

                $scope.mobile.saveCache();

                ShowMessage.showMessage(data.data.message, function () {
                    $state.go('app.home');
                });
            }).catch(function (err) {
                return ShowMessage.showMessage(err, function () {});
            });
    };
});
