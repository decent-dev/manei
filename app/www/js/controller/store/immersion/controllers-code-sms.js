var app = angular.module('manei');

app.controller('CodeSMSCtrl', function ($scope, ManeiConfig, ShowMessage, $state, Mobile) {

    $scope.mobile = new Mobile();
    $scope.mobile.loadCache();

    delete $scope.mobile.confirmationCode;

    $scope.send = function (imersao) {

        $scope.mobile.saveCache();

        $scope.mobile.validateImersao()
            .then(function (data) {
                if (data.data.message == 'ok') {
                    $state.go('immersion.profile-step-1');
                }

            }).catch(function (err) {
                return ShowMessage.showMessage(err, function () {});
            });

    };
});
