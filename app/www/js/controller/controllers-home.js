var app = angular.module('manei');

app.controller('HomeCtrl', function ($scope, $stateParams, $state, Authentication, Mobile) {

    $scope.user = new Authentication().getUser();

    var mobile =  new Mobile();
    mobile.loadCache();
    $scope.mobile = mobile;   
});
