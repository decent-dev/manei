var app = angular.module('manei');

app.factory('Workflow', function ($q, Http, ShowMessage, Queue, Storage) {

    var workflowSelect = new Storage('workflow');

    var workflows = [];
    workflows.push({
        code: 1,
        description: 'Tests WF',
        steps: ['app.order-resume', 'app.kkkkkk']
    });

    var Workflow = function () {
        this.step = 0;
        var storage = workflowSelect.get();
        if (storage) {
            for (var i in storage) {
                this[i] = storage[i];
            }
        }
    };

    Workflow.prototype.load = function (code) {
        for (var i in workflows) {
            var workflow = workflows[i];
            if (workflow.code = code) {
                for (var i in workflow) {
                    this[i] = workflow[i];
                }
                return this;
            }
        }
    };

    Workflow.prototype.save = function () {
        return workflowSelect.save(this);
    };

    Workflow.prototype.nextStep = function () {
        return this.steps[this.step++];
    };

    return Workflow;
});
