var app = angular.module('manei');

app.factory('Order', function (Authentication, MD5, $q, Domain, Queue, ShowMessage, Storage, Person, Http) {

    var auth = new Authentication();

    var storage = new Storage('order');

    var queue = new Queue(['order', 'queue'].join('/'));
    queue.service();

    var queueAccept = new Queue(['order', 'accept'].join('/'));
    queueAccept.service();

    var queueReject = new Queue(['order', 'reject'].join('/'));
    queueReject.service();

    var queueDone = new Queue(['order', 'done'].join('/'));
    queueDone.service();

    var Order = function () {
        Domain.apply(arguments);
        this.domainName = 'order';
        if (!this.itens) this.itens = [];
    };

    Order.prototype = Object.create(Domain.prototype);

    Order.prototype.create = function (order) {
        Domain.prototype.create.call(this, order);
        this.getTotal();
        return this;
    };

    Order.prototype.removeItem = function (index) {
        if (this.IDHash) {
            throw 'Pedido fechado'
        }
        this.itens.splice(index, 1);
        this.getTotal();
    };

    Order.prototype.save = function () {
        var $this = this;
        return $q(function (resolve, reject) {
            storage.save($this);
        });
    };

    Order.prototype.get = function () {
        var $this = this;
        return $q(function (resolve, reject) {
            Order.prototype.create.call($this, storage.get($this));
        });
    };

    Domain.prototype.resolveNextPageByPersonFrom = function (limit, orderBy) {

        var cache = new Storage(['cache', this.domainName, 'list'].join('.'));

        if (!this.skip) this.skip = 0;

        var $this = this;

        return $q(function (resolve, reject) {
            Http.get([$this.domainName, 'pagination', 'personFrom', $this.skip, limit, orderBy].join('/'))
                .then(function (domains) {

                    var _domains = [];
                    for (var i in domains.data) {
                        var domain = domains.data[i];
                        _domains.push(new Domain().create(domain));
                    }
                    cache.save(_domains);
                    return resolve(_domains);
                }).catch(function (err) {
                    console.error(err);
                    var _domains = [];
                    var domains = cache.get();
                    if(!domains) return resolve([]);
                    for (var i in domains) {
                        var domain = domains[i];
                        _domains.push(new Domain().create(domain));
                    }
                    return resolve(_domains);
                });

            $this.skip += limit;
        });
    };

    Domain.prototype.resolveNextPageByPersonTo = function (limit, orderBy) {

        var cache = new Storage(['cache', this.domainName, 'list'].join('.'));

        if (!this.skip) this.skip = 0;

        var $this = this;

        return $q(function (resolve, reject) {
            Http.get([$this.domainName, 'pagination', 'personTo', $this.skip, limit, orderBy].join('/'))
                .then(function (domains) {

                    var _domains = [];
                    for (var i in domains.data) {
                        var domain = domains.data[i];
                        _domains.push(new Domain().create(domain));
                    }
                    cache.save(_domains);
                    return resolve(_domains);
                }).catch(function (err) {
                    console.error(err);
                    var _domains = [];
                    var domains = cache.get();
                    if(!domains) return resolve([]);
                    for (var i in domains) {
                        var domain = domains[i];
                        _domains.push(new Domain().create(domain));
                    }
                    return resolve(_domains);
                });

            $this.skip += limit;
        });
    };

    Order.prototype.closeOrder = function () {
        var $this = this;
        return $q(function (resolve, reject) {
            var _from;
            for (var i in $this.itens) {
                var item = $this.itens[i];
                if (!_from) to = item.from;
                if (_from = !item.from) {
                    return reject('Existe Clientes divergentes nesse Pedido');
                }
            }

            var to;
            for (var i in $this.itens) {
                var item = $this.itens[i];
                if (!to) to = item.to;
                if (to = !item.to) {
                    return reject('Existe Estabelecimentos divergentes nesse Pedido');
                }
            }

            var timestamp = new Date().getTime();

            var id = [_from, to, timestamp].join('$');

            $this.IDHash = new MD5().encode(id);

            return resolve($this.IDHash);
        });
    };

    Order.prototype.pay = function () {
        var $this = this;

        var person = new Person();
        person.loadCache();

        return $q(function (resolve, reject) {

            ShowMessage.showInput("Digite o PIN de segurança", function (res) {

                var pinOff = person.password;

                if (pinOff != res)
                    return reject('Validação de PIN Off line não confere');

                $this.closeOrder()
                    .then(function (IDHash) {

                        var user = new Authentication().getUser();

                        var _from = user.id;

                        var to;
                        for (var i in $this.itens) {
                            if (!to) {
                                to = $this.itens[i].to;
                            }
                            if (to != $this.itens[i].to) {
                                return reject('Item com Estabelecimento diferente');
                            }
                        }

                        $this.queue();

                        //Assumindo que as vendas são Pos-pago/Crédito
                        user.balanceLimite -=  $this.getTotal();
                        user.saveCache();

                        return resolve();

                    }).catch(function (err) {
                        return reject(err);
                    });

            }, function () {
                return reject("PIN não foi digitado");
            });
        });
    };

    Order.prototype.addItem = function (item) {
        var $this = this;
        return $q(function (resolve, reject) {
            if ($this.IDHash) {
                return reject('Pedido fechado');
            }

            var user = auth.getUser();

            if (!item.date) {
                item.date = new Date();
            };

            if ('x' == item.from) {
                item.from = user.id;
            }

            $this.itens.push(item);
            $this.getTotal();

            return resolve();
        });
    };

    Order.prototype.queue = function () {
        queue.add(this);
    };

    Order.prototype.accept = function () {
        queueAccept.add(this);
    };

    Order.prototype.done = function () {
        queueDone.add(this);
    };

    Order.prototype.reject = function () {
        queueReject.add(this);
    };

    Order.prototype.addItemAndSort = function (item) {
        var $this = this;
        return $q(function (resolve, reject) {
            Order.prototype.addItem.call($this, item)
                .then(function () {
                    $this.itens.sort(function (a1, a2) {
                        if (typeof a2.date == 'string') a2.date = new Date(a2.date);
                        if (typeof a1.date == 'string') a1.date = new Date(a1.date);
                        return a2.date.getTime() - a1.date.getTime();
                    });
                    $this.getTotal();

                    return resolve();
                }).catch(function (err) {
                    return reject(err);
                });
        });
    };

    Order.prototype.getTotal = function () {
        this.total = this.itens.reduce(function (antes, agora) {
            return parseFloat(agora.amount) + antes
        }, 0);
        return this.total;
    };

    return Order;
});
