var app = angular.module('manei');

app.factory('Product', function ($q, Http, ShowMessage, Queue, Domain, Authentication) {

    var auth = new Authentication();

    var queue = new Queue(['product', 'queue'].join('/'));

    queue.service();

    var Product = function () {
        Domain.apply(arguments);
        this.domainName = 'product';

        if (!this.itens) this.itens = [];
    };

    Product.prototype = Object.create(Domain.prototype);

    Product.prototype.create = function (product) {
        if (!product) return;
        Domain.prototype.create.call(this, product);
        this.person = auth.getUser().id;
        return this;
    };

    Product.prototype.attachAmount = function (itens) {
        var $this = this;
        return this.resolveAll()
            .then(function (catalog) {

                for (var i in itens) {
                    var item = itens[i];

                    var products = catalog.filter(function (o) {
                        if (o.code == item.code) {
                            return o;
                        }
                    });

                    if (products.length == 1) {
                        var product = products[0];

                        if (product.amount)
                            item.amount = product.amount;

                        if (product.description)
                            item.description = product.description;
                    }
                }
                return $this;
            });
    };

    Product.prototype.queue = function () {
        queue.add(this);
    };

    return Product;
});
