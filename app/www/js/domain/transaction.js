var app = angular.module('manei');

app.factory('Transaction', function ($q, Http, ShowMessage, Queue, Storage) {

    var queue = new Queue(['transaction', 'queue'].join('/'));

    queue.service();

    var cache = new Storage('cache.transactions');

    var Transaction = function () {
    };

    Transaction.prototype.create = function (transaction) {
        if (!transaction) return;
        for (var i in transaction) {
            this[i] = transaction[i];
        }
        return this;
    };

    Transaction.prototype.load = function (id) {
      var $this = this;
        Http.get(['transaction', id].join('/'))
        .then(function (transaction) {
            $this.create(transaction.data);
        }).catch(function (err) {
            console.error(err);
            var transactions = cache.get();
            for (var i in transactions) {
                var transaction = transactions[i];
                if (transaction._id == _id) {
                    return $this.create(transaction);
                }
            }
        });
    };

    Transaction.prototype.resolveAll = function () {
      var $this = this;
      return $q(function (resolve, reject) {
          Http.get('transaction')
          .then(function (transactions) {
              var _transactions = [];
              for(var i in transactions.data){
                  var transaction = transactions.data[i];
                  _transactions.push(new Transaction().create(transaction));
              }
              cache.save(_transactions);
              return resolve(_transactions);
          }).catch(function (err) {
              console.error(err);
              var _transactions = [];
              var transactions = cache.get();
              for (var i in transactions) {
                  var transaction = transactions[i];
                  _transactions.push(new Transaction().create(transaction));
              }
              return resolve(_transactions);
          });
      });
    };

    Transaction.prototype.queue = function () {
        queue.add(this);
    };

    Transaction.prototype.save = function () {
        var $this = this;
        return $q(function (resolve, reject) {
            Http.post('transaction', this)
            .then(function (data) {

            })
            .catch(function (err) {
              console.error(err);
              return ShowMessage.showMessage(err, function () {});
            });
        });
    };

    Transaction.prototype.resolveNextPage = function (limit, orderBy) {

      if(!this.skip) this.skip = 0;

      var $this = this;

      return $q(function (resolve, reject) {
          Http.get(['transaction', 'pagination', $this.skip, limit, orderBy].join('/'))
          .then(function (transactions) {

              var _transactions = [];
              for(var i in transactions.data){
                  var transaction = transactions.data[i];
                  _transactions.push(new Transaction().create(transaction));
              }
              cache.save(_transactions);
              return resolve(_transactions);
          }).catch(function (err) {
              console.error(err);
              var _transactions = [];
              var transactions = cache.get();
              for (var i in transactions) {
                  var transaction = transactions[i];
                  _transactions.push(new Transaction().create(transaction));
              }
              return resolve(_transactions);
          });

          $this.skip += limit;
      });
    };


    return Transaction;
});
