var app = angular.module('manei');

app.factory('Mobile', function ($q, Http, ShowMessage, Queue, Domain, Authentication) {

    var Mobile = function () {
        Domain.apply(arguments);
        this.domainName = 'mobile';
    };

    Mobile.prototype = Object.create(Domain.prototype);

    Mobile.prototype.imersao = function () {
        return Http.post('/imersao', this);
    };

    Mobile.prototype.validateImersao = function () {
        return Http.post('/imersaovalidate', this);
    };

    Mobile.prototype.imersaoSeason = function () {
        return Http.post('/imersaoSeason', this);
    };

    Mobile.prototype.saveSeason = function () {
        return Http.post('/mobile/season', this);
    };

    Mobile.prototype.registerGCM = function (device_token) {
        return Http.post([this.domainName, 'registerGCM'].join('/'), {
                'device_token': device_token
            }, true)
            .then(function (response) {
                return response;
            });
    };

    return Mobile;
});
