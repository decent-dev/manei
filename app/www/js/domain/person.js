var app = angular.module('manei');

app.factory('Person', function ($q, Http, ShowMessage, Queue, Domain) {

    var Person = function () {
        Domain.apply(arguments);
        this.domainName = 'person';
    };

    Person.prototype = Object.create(Domain.prototype);

    Person.prototype.existsByCpfCnpj = function () {
        return Http.get([this.domainName, 'existsByCpfCnpj', this.cpfCnpj].join('/'));
    };

    Person.prototype.getBalancePos = function () {
        return Http.get([this.domainName, 'getbalancepos'].join('/'));
    };

    Person.prototype.getBalanceLimite = function () {
        return Http.get([this.domainName, 'getbalancelimite'].join('/')).then(function (response) {
            return response.data.ammount;
        });
    };
    
    Person.prototype.resumoTransactionReceitaPos = function () {
        return Http.get([this.domainName, 'resumoReceitaPos'].join('/')).then(function (response) {
            return response.data;
        });
    };

    Person.prototype.getBalanceSaldo = function () {
        return Http.get([this.domainName, 'getbalancesaldo'].join('/')).then(function (response) {
            return response.data.ammount;
        });
    };

    return Person;
});
