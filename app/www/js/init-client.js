angular.module('manei', [
    'ionic',    
    'ui.mask'
])

        .run(function ($ionicPlatform) {
            $ionicPlatform.ready(function () {

                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);

                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        })

        .config(function ($stateProvider, $urlRouterProvider) {

            $stateProvider

                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'templates/client/menu.html',
                        controller: 'AppCtrl'
                    })

                    .state('app.home', {
                        url: '/home',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/requestList.html',
                                controller: 'RequestListCtrl'
                            }
                        }
                    })

                    .state('app.profile', {
                        url: '/profile',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/profile.html',
                                controller: 'ProfileClientCtrl'
                            }
                        }
                    })
                    .state('app.transactions', {
                        url: '/transactions',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/transactions.html',
                                controller: 'TransactionsCtrl'
                            }
                        }
                    })



                    .state('app.order', {
                        url: '/order',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/workflow/init-workflow.html',
                                controller: 'InitWorkFlowCtrl'
                            }
                        }
                    })

                    .state('app.order-resume', {
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/workflow/order-resume.html',
                                controller: 'OrderResumeCtrl'
                            }
                        }
                    })

                    .state('app.balance', {
                        url: '/balance',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/balance.html',
                                controller: 'BalanceCtrl'
                            }
                        }
                    })

                    .state('app.requestList', {
                        url: '/requestList',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/requestList.html',
                                controller: 'RequestListCtrl'
                            }
                        }
                    })

                    .state('app.requestOrder', {
                        url: '/requestOrder/:id',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/requestOrder.html',
                                controller: 'RequestOrderCtrl'
                            }
                        }
                    })

                    .state('immersion', {
                        url: '/init',
                        abstract: true,
                        templateUrl: 'templates/client/immersion/init.html'
                    })

                    .state('immersion.welcome', {
                        //url: '/welcome',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/immersion/welcome.html',
                                controller: 'WelcomeCtrl'
                            }
                        }
                    })

                    .state('immersion.phonenumber', {
                        //url: '/phonenumber',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/immersion/phonenumber.html',
                                controller: 'PhoneNumberCtrl'
                            }
                        }
                    })

                    .state('immersion.codesms', {
                        //url: '/codesms',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/immersion/codesms.html',
                                controller: 'CodeSMSCtrl'
                            }
                        }
                    })

                    .state('immersion.profile', {
                        //url: '/profile',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/client/immersion/profile.html',
                                controller: 'ProfileCtrl'
                            }
                        }
                    });

            // redireciona para tela home se autenticado
            // controlado com localStorage

            $urlRouterProvider.otherwise('/app/home');

        });
