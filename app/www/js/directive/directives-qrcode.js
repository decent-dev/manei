var app = angular.module('manei');

app.directive('qrcode', function () {
    return {
        restrict: 'A',
        scope: {
            qrcode: '=qrcode'
        },
        link: function (scope, element, attr) {
            scope.$watch('qrcode', function () {
                if (!scope.qrcode) return;
                console.log(scope.qrcode);
                var qrcode = new QRCode(element[0]).makeCode(scope.qrcode);
            });
        }
    };
});
