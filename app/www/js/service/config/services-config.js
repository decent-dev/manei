var app = angular.module('manei');

app.service('ManeiConfig', function () {

    var _enum = {};

    _enum.DEVELOPER = 'dev';
    _enum.PRODUTION = 'prod';
    _enum.CLIENT = 'client';
    _enum.STORE = 'store';

    return {
        gcmID : 'manei-1128',
        host: 'http://app.manei.com.br:443/',
        env: 'prod',
        target: 'store',
        ENV: _enum,
        pctMax: 3,
        queuePooling: 1000 * 1,
        senderID : '749238628474'
    };
});
