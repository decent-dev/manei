var app = angular.module('manei');

app.service('ProtocolProduct', function (Authentication, ZIP, Adler32) {

    var user = new Authentication().getUser();

    var zip = new ZIP();

    var adler32 = new Adler32();

    var ProtocolProduct = function () {};

    ProtocolProduct.prototype.decode = function (str) {

        str = zip.decompress(str);

        //var regex = /^(\w{1,24})\|(\w{1,24})\|(\d{1,})\|(.*)$/;
        var regex = /^(x)\|(\w{1,24})\|(\d{1,})\|(\d{1,})\|(.*)\|(\d{1,})\$(.*)$/;
        if (regex.test(str)) {
            var obj = {};
            obj.from = str.replace(regex, "$1");
            obj.to = str.replace(regex, "$2");
            obj.code = str.replace(regex, "$3").replace(/ /g, '');
            var samount = str.replace(regex, "$4").replace(/ /g, '');
            obj.amount = parseFloat(samount) / 100;
            obj.description = decodeURI(str.replace(regex, "$5"));
            obj.workflow = str.replace(regex, "$6");
            obj.checksum = str.replace(regex, "$7");

            if(getChecksum(makeProtocol(obj)) != obj.checksum){
                return undefined;
            }

            return obj;
        }
        return undefined;
    };

    var getChecksum = function (protocol) {
      return adler32.encode(protocol);
    };

    var makeProtocol = function (obj) {
      var protocolList = [obj.from, obj.to, obj.code, Math.round(obj.amount * 100), encodeURI(obj.description), obj.workflow];
      return protocolList.join('|');
    }

    ProtocolProduct.prototype.encode = function (obj) {

        if (!obj.from) obj.from = 'x';

        if (!obj.to) obj.to = user.id;

        var protocol = makeProtocol(obj);

        var checksum = getChecksum(protocol)

        var protocol = [protocol, checksum].join('$')

        protocol = zip.compress(protocol);

        return protocol;

    };

    return ProtocolProduct;
});
