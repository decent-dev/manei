var app = angular.module('manei');

app.service('GCMService', function (ShowMessage, Mobile, $ionicPlatform, ManeiConfig, NotificationAlert) {
    $ionicPlatform.ready(function () {

        if (ManeiConfig.env == ManeiConfig.ENV.PRODUTION) {

            pushNotification = window.plugins.pushNotification;

            window.onNotification = function (e) {

                console.log('notification received');

                switch (e.event) {
                case 'registered':
                    if (e.regid.length > 0) {

                        var mobile = new Mobile();

                        var device_token = e.regid;

                        mobile.registerGCM(device_token)
                            .then(function (response) {
                                console.log('registered!', device_token);
                            });
                    }
                    break;

                case 'message':
                    // ShowMessage.showMessage('msg received', function () {});
                    // alert(JSON.stringify(e));
                    NotificationAlert.qtdeNotification++;
                    break;

                case 'error':
                    ShowMessage.showMessage('error occured', function () {});
                    break;

                }
            };

            window.errorHandler = function (error) {
                ShowMessage.showMessage(error, function () {});
            }

            pushNotification.register(
                onNotification,
                errorHandler, {
                    'badge': 'true',
                    'sound': 'true',
                    'alert': 'true',
                    'senderID': ManeiConfig.senderID,
                    'ecb': 'onNotification'
                }
            );

        } else {
            console.log('env = dev para registro GCM');
        }
    });
    return
});
