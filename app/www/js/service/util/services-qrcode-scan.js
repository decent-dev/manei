var app = angular.module('manei');

app.service('QRCodeScan', function (ManeiConfig, $q) {

    var QRCodeScan = function () {
    };

    QRCodeScan.prototype.scan = function () {
        return $q(function (resolve, reject) {
            if (ManeiConfig.env == ManeiConfig.ENV.PRODUTION) {
                cordova.plugins.barcodeScanner.scan(
                    function (result) {

                        if (result.cancelled) return;

                        if (!result.text) return;

                        if (result.format != 'QR_CODE') return;

                        return resolve(result.text);
                    },
                    function (err) {
                        return reject(err);
                    }
                );
            } else {
                return resolve(prompt('Você não tem camera, digite o protocol, modo debug'));
            }
        });
    };

    return QRCodeScan;

});
