var app = angular.module('manei');

app.service('Adler32', function () {

    var Adler32 = function () {};

    Adler32.prototype.encode = function (a) {
        for (var b = 65521, c = 1, d = 0, e = 0, f; f = a.charCodeAt(e++); d = (d + c) % b) c = (c + f) % b;
        return (d << 16 | c).toString(16);
    };

    return Adler32;

});
