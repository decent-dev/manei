var app = angular.module('manei');

app.service('ShowMessage', function ($ionicPopup) {
    var showMessage = function (message, cbOk, cbCancel, title, subTitle) {
        console.info(message);
        var buttons = [];

        if (cbOk && cbCancel) {
            buttons = [{
                text: 'Cancel',
                onTap: function (e) {
                    cbCancel(e);
                }
            }, {
                text: '<b>Ok</b>',
                type: 'button-positive',
                onTap: function (e) {
                    cbOk(e);
                }
            }];
        } else if (cbOk) {
            buttons = [{
                text: '<b>Ok</b>',
                type: 'button-positive',
                onTap: function (e) {
                    cbOk(e);
                }
            }];
        }

        // An elaborate, custom popup
        return $ionicPopup.show({
            template: '<span>' + message || $scope.msg + '</span>',
            title: title || 'Mensagem',
            subTitle: subTitle || '',
            buttons: buttons
        });
    };

    var showInput = function (message, cbOk, cbCancel, title, subTitle) {
      console.info(message);
        // An elaborate, custom popup
        return $ionicPopup.prompt({
            title: title || 'Mensagem',
            template: message,
            inputType: 'tel',
            inputPlaceholder: '...',
            focusFirstInput : true
        }).then(function (res) {

            if(res != undefined && cbOk) cbOk(res);

            if(res == undefined && cbCancel) cbCancel(res);

        });
    };
    
    var showInputText = function (message, text, cbOk, cbCancel, title, subTitle) {
      console.info(message);
        // An elaborate, custom popup
        return $ionicPopup.prompt({
            title: title || 'Mensagem',
            template: message,
            inputType: 'text',
            inputPlaceholder: text || '...'
        }).then(function (res) {

            if(res != undefined && cbOk) cbOk(res);

            if(res == undefined && cbCancel) cbCancel(res);

        });
    };

    return {
        showMessage: showMessage,
        showInput : showInput,
        showInputText: showInputText
    }

});
