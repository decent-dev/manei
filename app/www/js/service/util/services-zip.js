var app = angular.module('manei');

app.service('ZIP', function () {

    var ZIP = function () {};

    ZIP.prototype.compress = function (string) {
        return string;//LZString.compress(string);
    };

    ZIP.prototype.decompress = function (string) {
        return string;//LZString.decompress(string);
    };

    return ZIP;

});
