var app = angular.module('manei');

app.service('Queue', function (Storage, Http, ManeiConfig) {

    var Queue = function (_type) {
        this.type = _type;
        this.queue = ['manei', 'queue', _type].join('.');
        this.storage = new Storage(this.queue);
        this.lock = false;
    };

    Queue.prototype.service = function () {

        var $this = this;

        setTimeout(function () {
            try {
                $this.sync();
            } catch (e) {
                console.error(this.queue, e);
            } finally {
                $this.service();
            }
        }, ManeiConfig.queuePooling);
    };

    Queue.prototype.add = function (obj) {
        this.lock = true;
        var queue = this.storage.get() || [];
        queue.push(obj);
        this.storage.save(queue);
        this.lock = false;
    };

    Queue.prototype.sync = function () {
        if (this.lock) {
            console.log(this.queue, 'service locked');
            return;
        }
        var queue = this.storage.get() || [];

        if (queue.length == 0) return;

        var o = queue.pop();
        this.storage.save(queue);

        var $this = this;

        Http.post(this.type, o, true)
            .then(function (data) {

            }).catch(function (err) {
                queue.push(o);
                $this.storage.save(queue);
            });
    };

    return Queue;
});
