var app = angular.module('manei');

app.filter('timeago', function (TimeAgo) {
    return function (datetime) {

        return TimeAgo.ago(datetime);
    };
});
