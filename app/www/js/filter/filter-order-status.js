var app = angular.module('manei');

app.filter('statusorderlabel', function () {
    return function (status) {

        if (!status || !status.reduce) return 'Status Inválido';

        var s = status.reduce(function (ant, ago) {

            if (!ant.dtcreate) return ago;

            return (new Date(ago.dtcreate).getTime() > new Date(ant.dtcreate).getTime() || ago.code > ant.code ? ago : ant);
        }, {});

        return s.label;
    };
});


app.filter('statusordercode', function () {
    return function (status) {

        if (!status || !status.reduce) return 0;

        var s = status.reduce(function (ant, ago) {

            if (!ant.dtcreate) return ago;

            return (new Date(ago.dtcreate).getTime() > new Date(ant.dtcreate).getTime() || ago.code > ant.code ? ago : ant);
        }, {});

        return s.code;
    };
});
