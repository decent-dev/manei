inject.define("manei.routes.transaction", [
    "restifyWeb.restifyWeb",
    "manei.utils.util",
    "kueQueue.kueQueue",
    "redisCache.api",
    "manei.model.mobile",
    "manei.model.person",
    "blockio.api",
    "kueQueue.utils.util",
    "manei.routes.util.util",
    "manei.rule.person",
    "manei.rule.transaction",
    "logApi.log",
    "manei.rule.order",
    function (restifyWeb, util, kueQueue, redisCache, Mobile, Person, blockio, logQueue, utilRouter, profile, transactionRule, console, orderRule) {
        var self = {};

        var restify = require('restify');


        self.getAll = function (req, res, next) {
            console.log('getAllTransaction');
            transactionRule.resolveTransactions()
                .then(function (transactions) {
                    return res.send(transactions);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPagination = function (req, res, next) {
            console.log('getPaginationTransaction');
            var iduser = req.headers.iduser;
            if (!iduser) {
                var msgtrace = ['Não foi passado o ID'].join(' ');
                console.trace(msgtrace);
                return next(util.errorResponse(msgtrace));
            }

            transactionRule.resolveTransactionsByPersonId(iduser)
                .then(function (data) {
                    return res.send(data);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(util.errorResponse(err));
                });
        };

        self.getPaginationOrderBy = function (req, res, next) {
            console.log('getPaginationTransaction');
            var iduser = req.headers.iduser;
            if (!iduser) {
                var msgtrace = ['Não foi passado o ID'].join(' ');
                console.trace(msgtrace);
                return next(util.errorResponse(msgtrace));
            }

            transactionRule.resolveTransactionsByPersonId(iduser)
                .then(function (data) {
                    return res.send(data);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(util.errorResponse(err));
                });
        };

        self.getById = function (req, res, next) {
            console.log('getTransactionById for', req.params.id);
            transactionRule.resolveTransactionById(req.params.id)
                .then(function (transaction) {
                    return res.send(transaction);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.save = function (req, res, next) {
            console.log('Salvando transaction .... ');
            transactionRule.save(req.body)
                .then(function (transaction) {
                    console.log('Transaction salved!!! ID:', transaction._id);
                    return next();
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.queue = function (req, res, next) {
            console.log('Queue transaction .... ');

            var job = kueQueue.create('transaction', req.body)
            .save(function (err) {
                if (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                }
                console.log('Queued transaction');
                next();
            });

            logQueue.logQueue(job);
        };

        restifyWeb.get('/transaction/:id', utilRouter.authentication,
            self.getById);

        restifyWeb.get('/transaction', utilRouter.authentication,
                self.getAll);

        restifyWeb.get('/transaction/pagination/:skip/:limit', utilRouter.authentication,
                self.getPagination);

        restifyWeb.get('/transaction/pagination/:skip/:limit/:orderBy', utilRouter.authentication,
                self.getPaginationOrderBy);

        restifyWeb.post('/transaction', utilRouter.authentication,
                self.save,
                utilRouter.doneOK);

        restifyWeb.post('/transaction/queue', utilRouter.authentication,
                self.queue,
                utilRouter.doneOK);

        return self;
    }
]);
