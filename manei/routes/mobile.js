inject.define("manei.routes.mobile", [
    "logApi.log",
    "restifyWeb.restifyWeb",
    "manei.rule.mobile",
    "manei.routes.util.util",
    "kueQueue.kueQueue",
    "kueQueue.utils.util",
    "manei.utils.util",
    "redisCache.api",
    "manei.rule.person",
    "manei.model.person",
    function (console, restifyWeb, mobileRule, utilRouter, kueQueue, logQueue, util, redisCache, personRule, Person) {
        var self = {};

        var restify = require('restify');

        self.getAll = function (req, res, next) {
            console.log('getAllMobile');
            mobileRule.resolveMobiles()
                .then(function (mobiles) {
                    return res.send(mobiles);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPagination = function (req, res, next) {
            console.log('getPaginationMobile');
            mobileRule.resolvePaginationMobiles(req.params.skip, req.params.limit)
                .then(function (mobiles) {
                    return res.send(mobiles);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPaginationOrderBy = function (req, res, next) {
            console.log('getPaginationMobile');
            mobileRule.resolvePaginationOrderByMobiles(req.params.skip, req.params.limit, req.params.orderBy)
                .then(function (mobiles) {
                    return res.send(mobiles);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getById = function (req, res, next) {
            console.log('getMobileById for', req.params.id);
            mobileRule.resolveMobileById(req.params.id)
                .then(function (mobile) {
                    return res.send(mobile);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.saveImersaoSeason = function (req, res, next) {

            var mobile = req.body;
            var person = mobile.person;

            console.log('Usuário', '(' + person.cpfCnpj + ')', 'Imergindo Season .... ');

            Person
                .findByCpfCnpj(person.cpfCnpj)
                .then(function (person) {
                    personRule.getMobileMasterByCpfCnpj(person.cpfCnpj)
                        .then(function (mobileMaster) {

                            var confirmationCodeSeason = util.parseConfirmationCode(req.body.confirmationCodeSeason);

                            confirmationCodeSeason = [confirmationCodeSeason.prefix, confirmationCodeSeason.sulfix].join('-');

                            redisCache.get(mobileMaster.phoneNumber)
                                .then(function (code) {
                                    if (code) {
                                        console.log('Codigo digitado', confirmationCodeSeason, 'Codigo esperado', code);
                                        if (code == confirmationCodeSeason) {

                                            console.log('Expirando Codigo do Redis');
                                            redisCache.expire(mobileMaster.phoneNumber, 0);

                                            return mobileRule
                                                .createWalletForMobile(person.cpfCnpj, mobile.IMEI, mobile.phoneNumber, confirmationCodeSeason)
                                                .then(function (mobile) {
                                                    if (!mobile) {
                                                        var msgtrace = ['Não pode criar Mobile representation'].join(' ');
                                                        console.trace(msgtrace);
                                                        return next(utilRouter.errorResponse(msgtrace));
                                                    }

                                                    var msg = ['Dados de ', person.name, ' foi salvo com sucesso'].join(' ');
                                                    console.log(msg);
                                                    res.send({
                                                        message: msg,
                                                        authenticationCode: [person._id, mobile._id].join('-'),
                                                        idUser: person._id
                                                    });
                                                })
                                                .catch(function (err) {
                                                    console.trace(err);
                                                    return next(utilRouter.errorResponse(err));
                                                });

                                        } else {
                                            var msgtrace = ['Código de verificação Inválido'].join(' ');
                                            console.trace(msgtrace);
                                            return next(utilRouter.errorResponse(msgtrace));
                                        }
                                    } else {
                                        var msgtrace = ['Não existe um pedido de Imersão ou pedido de imersão expirado'].join(' ');
                                        console.trace(msgtrace);
                                        return next(utilRouter.errorResponse(msgtrace));
                                    }
                                })
                                .catch(function (err) {
                                    console.trace(err);
                                    return next(utilRouter.errorResponse(msgtrace));
                                });

                        }).catch(function (err) {
                            console.trace(err);
                            return next(utilRouter.errorResponse(err));
                        });
                }).catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.saveImersao = function (req, res, next) {
            console.log('Usuário', '(' + req.body.person.name + ')', 'Imergindo .... ');

            var mobile = req.body;
            var person = mobile.person;

            personRule
                .createPerson(person.name, person.cpfCnpj, person.password)
                .then(function (p) {

                    if (!p) {
                        var msgtrace = ['Não pode criar o registro na base de dados'].join(' ');
                        console.trace(msgtrace);
                        return next(utilRouter.errorResponse(msgtrace));
                    }

                    mobileRule
                        .createWalletForMobile(person.cpfCnpj, mobile.IMEI, mobile.phoneNumber, mobile.confirmationCode)
                        .then(function (mobile) {
                            if (!mobile) {
                                var msgtrace = ['Não pode criar Mobile representation'].join(' ');
                                console.trace(msgtrace);
                                return next(utilRouter.errorResponse(msgtrace));
                            }

                            var msg = ['Dados de ', p.name, ' foi salvo com sucesso'].join(' ');
                            console.log(msg);
                            res.send({
                                message: msg,
                                authenticationCode: [p._id, mobile._id].join('-'),
                                idUser: p._id
                            });
                        })
                        .catch(function (err) {
                            console.trace(err);
                            return next(utilRouter.errorResponse(err));
                        });

                }).catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.queue = function (req, res, next) {
            console.log('Queue mobile .... ');

            var job = kueQueue.create('mobile', req.body)
                .save(function (err) {
                    if (err) {
                        console.trace(err);
                        return next(utilRouter.errorResponse(err));
                    }
                    console.log('Queued mobile');
                    next();
                });

            logQueue.logQueue(job);
        };

        /**
         * Valida código de validação
         */
        self.validateConfirmationCode = function (req, res, next) {

            console.log('Confirmação Code', req.body.confirmationCode);

            var phoneNumber = req.body.phoneNumber;
            var confirmationCode = req.body.confirmationCode;

            var to = ['+', phoneNumber.country, phoneNumber.ddd, phoneNumber.prefix, phoneNumber.sulfix].join('');

            console.log('validateConfirmationCode', 'Para', to);

            confirmationCode = [confirmationCode.prefix, confirmationCode.sulfix].join('-');

            console.log('validateConfirmationCode', 'Com', confirmationCode);

            redisCache.get(to)
                .then(function (code) {
                    if (code) {
                        console.log('Codigo digitado', confirmationCode, 'Codigo esperado', code);
                        if (code == confirmationCode) {

                            console.log('Expirando Codigo do Redis');
                            redisCache.expire(to, 0);

                            return next();
                        } else {
                            var msgtrace = ['Código de verificação Inválido'].join(' ');
                            console.trace(msgtrace);
                            return next(utilRouter.errorResponse(msgtrace));
                        }
                    } else {
                        var msgtrace = ['Não existe um pedido de Imersão ou pedido de imersão expirado'].join(' ');
                        console.trace(msgtrace);
                        return next(utilRouter.errorResponse(msgtrace));
                    }
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(msgtrace));
                });
        };

        /**
         * Resolve padrão de telefone
         */
        self.parsePhoneNumber = function (req, res, next) {
            if (!req.body || !req.body.phoneNumber) {
                var msgtrace = ['phoneNumber não informado'].join(' ');
                console.trace(msgtrace);
                return next(utilRouter.errorResponse(msgtrace));
            }

            req.body.phoneNumber = util.parsePhoneNumber(req.body.phoneNumber);

            if (!req.body.phoneNumber) {
                var msgtrace = ['phoneNumber não é um padrão de numero de telefone'].join(' ');
                console.trace(msgtrace);
                return next(utilRouter.errorResponse(msgtrace));
            }

            return next();
        };

        /**
         * Resolve o padrão de Código de confirmação
         */
        self.parseConfirmatioCode = function (req, res, next) {
            if (!req.body || !req.body.confirmationCode) {
                var msgtrace = ['confirmationCode não informado'].join(' ');
                console.trace(msgtrace);
                return next(utilRouter.errorResponse(msgtrace));
            }

            req.body.confirmationCode = util.parseConfirmationCode(req.body.confirmationCode);

            if (!req.body.confirmationCode) {
                var msgtrace = ['confirmationCode não é um padrão de código de confirmação'].join(' ');
                console.trace(msgtrace);
                return next(utilRouter.errorResponse(msgtrace));
            }

            return next();
        };

        /**
         * Verifica se o codigo de confirmação existe
         */
        self.existsConfirmationCode = function (req, res, next) {
            var phoneNumber = req.body.phoneNumber;

            var to = ['+', phoneNumber.country, phoneNumber.ddd, phoneNumber.prefix, phoneNumber.sulfix].join('');

            console.log('Verificando se existe um codigo para', to);

            redisCache.get(to)
                .then(function (code) {
                    if (code) {
                        var msgtrace = ['Já existe um pedido de Imersão'].join(' ');
                        console.trace(msgtrace);
                        return next(utilRouter.errorResponse(msgtrace));
                    } else {
                        return next();
                    }
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(err);
                });
        };

        self.getMasterPhonenumber = function (req, res, next) {

            var cpfCnpj = req.body.cpfCnpj;

            personRule.getMobileMasterByCpfCnpj(cpfCnpj)
                .then(function (mobile) {
                    req.body.phoneNumber = mobile.phoneNumber;
                    return next();
                }).catch(function (err) {
                    console.trace(err);
                    return next(err);
                });
        };

        /**
         * Envia para a fila de SMS o codigo de imersão
         */
        self.queueSms = function (req, res, next) {
            var job = kueQueue.create('smsImersao', req.body.phoneNumber)
                .save(function (err) {
                    if (err) {
                        console.trace(err);
                        return next(utilRouter.errorResponse(err));
                    }
                    console.log('Queued SMS', req.body.phoneNumber);
                    return next();
                });

            logQueue.logQueue(job);
        };

        self.registerGCM = function (req, res, next) {
            console.log('registerGCM');

            var user = req.headers.iduser;
            var device_token = req.body.device_token;

            mobileRule.registerGCM(user, device_token)
                .then(function (mobiles) {
                    res.send(mobiles);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        restifyWeb.post('/mobile/registerGCM', utilRouter.authentication,
            self.registerGCM);

        restifyWeb.get('/mobile/:id', utilRouter.authentication,
            self.getById);

        restifyWeb.get('/mobile', utilRouter.authentication,
            self.getAll);

        restifyWeb.get('/mobile/pagination/:skip/:limit', utilRouter.authentication,
            self.getPagination);

        restifyWeb.get('/mobile/pagination/:skip/:limit/:orderBy', utilRouter.authentication,
            self.getPaginationOrderBy);

        restifyWeb.post('/mobile',
            self.saveImersao);

        restifyWeb.post('/mobile/queue', utilRouter.authentication,
            self.queue,
            utilRouter.doneOK);

        restifyWeb.post('/imersaoSeason',
            self.getMasterPhonenumber,
            self.queueSms,
            utilRouter.doneOK);

        restifyWeb.post('/mobile/season',
            self.saveImersaoSeason);

        /**
         * Rota para imersão do mobile
         * um vez invocada, gera e envia um codigo de validação para o numero de celular
         */
        restifyWeb.post('/imersao',
            self.parsePhoneNumber,
            self.existsConfirmationCode,
            self.queueSms,
            utilRouter.doneOK);

        /**
         * Rota para validação do codigo enviado
         * um vez invocada, valida o codigo e cria uma conta na base de dados local
         */
        restifyWeb.post('/imersaovalidate',
            self.parsePhoneNumber,
            self.parseConfirmatioCode,
            self.validateConfirmationCode,
            utilRouter.doneOK);

        return self;
    }
]);
