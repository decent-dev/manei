inject.define("manei.routes.person", [
    "restifyWeb.restifyWeb",
    "kueQueue.kueQueue",
    "redisCache.api",
    "manei.model.mobile",
    "manei.model.person",
    "blockio.api",
    "kueQueue.utils.util",
    "manei.routes.util.util",
    "manei.rule.person",
    "manei.rule.transaction",
    "logApi.log",
    "manei.rule.mobile",
    "manei.rule.limit",
    function (restifyWeb, kueQueue, redisCache, Mobile, Person, blockio, logQueue, utilRouter, personRule, transactionRule, console, mobileRule, limitRule) {
        var self = {};

        var restify = require('restify');

        /**
         * getBalanceLimite
         */
        self.getBalanceLimite = function (req, res, next) {
            console.log('Get Balance Limite for', req.headers.iduser);

            limitRule.resolveLimitByPerson(req.headers.iduser)
                    .then(function (limit) {
                        return res.send(limit);
                    })
                    .catch(function (err) {
                        console.trace(err);
                        return next(utilRouter.errorResponse(err));
                    });
        };

        /**
         * getBalanceSaldo
         */
        self.getBalanceSaldo = function (req, res, next) {
            console.log('Get Balance Saldo for', req.headers.iduser);

            limitRule.resolveSaldoPagarByPerson(req.headers.iduser)
                    .then(function (limit) {
                        return res.send(limit);
                    })
                    .catch(function (err) {
                        console.trace(err);
                        return next(utilRouter.errorResponse(err));
                    });
        };
        
        /**
         * resumoTransactionReceita
         */
        self.resumoTransactionReceitaPos = function (req, res, next) {
            console.log('resumoTransactionReceita', req.headers.iduser);

            personRule.resumoTransactionReceitaPos(req.headers.iduser)
                    .then(function (resumo) {
                        return res.send(resumo);
                    })
                    .catch(function (err) {
                        console.trace(err);
                        return next(utilRouter.errorResponse(err));
                    });
        };

        /**
         * getUserName
         */
        self.getUserName = function (req, res, next) {
            console.log('getUserName for', req.params.id);
            personRule.getUserName(req.params.id)
                    .then(function (name) {
                        return res.send({
                            name: name
                        });
                    })
                    .catch(function (err) {
                        console.trace(err);
                        return next(utilRouter.errorResponse(err));
                    });
        };

        self.getByCpfCnpj = function (req, res, next) {
            console.log('getByCpfCnpj for', req.params.cpfCnpj);
            Person.findByCpfCnpj(req.params.cpfCnpj)
                    .then(function (person) {
                        if (person) {
                            return res.send({
                                exists: true
                            });
                        } else {
                            return res.send({
                                exists: false
                            });
                        }

                    })
                    .catch(function (err) {
                        console.trace(err);
                        return next(utilRouter.errorResponse(err));
                    });
        };

        /**
         * Rota para validação do codigo enviado
         * um vez invocada, valida o codigo e cria uma conta na base de dados local
         */

        restifyWeb.get('/person/existsByCpfCnpj/:cpfCnpj',
                self.getByCpfCnpj);

        restifyWeb.get('/person/getbalancelimite', utilRouter.authentication,
                self.getBalanceLimite);

        restifyWeb.get('/person/getbalancesaldo', utilRouter.authentication,
                self.getBalanceSaldo);

        restifyWeb.get('/getUserName/:id', utilRouter.authentication,
                self.getUserName);
        
        restifyWeb.get('/person/resumoReceitaPos', utilRouter.authentication,
                self.resumoTransactionReceitaPos);
                

        return self;
    }
]);
