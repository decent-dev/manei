inject.define("manei.routes.util.util", [
    "manei.utils.util",
    "redisCache.api",
    "manei.model.mobile",
    "manei.model.person",
    "blockio.api",
    "logApi.log",
    function (util, redisCache, Mobile, Person, blockio, console) {
        var self = {};

        var restify = require('restify');

        self.trataErroRouter = function (err) {
            console.error(JSON.stringify(err));
            if (typeof err == 'string') return err;
            if (err.message) return err.message;
        };

        self.errorResponse = function (msg) {
            return new restify.InternalServerError(self.trataErroRouter(msg));
        };

        /**
         * Done OK
         */
        self.authentication = function (req, res, next) {
            var imei = req.headers.imei;
            var auth = req.headers.auth;

            if (!imei || !auth) return next(self.errorResponse('Acesso sem credenciais'));

            Mobile.findByIMEI(imei, function (err, mobile) {
                if (err) {
                    console.trace(err);
                    return next(err);
                }

                if (!mobile) {
                    var msgtrace = ['Mobile não encontrado para autenticação'].join(' ');
                    console.trace(msgtrace);
                    return next(self.errorResponse(msgtrace));
                }

                if (mobile.auth != auth) {
                    var msgtrace = ['Mobile não authenticado'].join(' ');
                    console.trace(msgtrace);
                    return next(self.errorResponse(msgtrace));
                }

                return next();
            });
        };

        /**
         * Done OK
         */
        self.doneOK = function (req, res, next) {
            return res.send({
                message: "ok"
            });
        };

        return self;
    }
]);
