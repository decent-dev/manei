inject.define("manei.consumers.person", [
    "logApi.log",
    "kueQueue.kueQueue",
    "manei.rule.mobile",
    "GCM.gcm",
    "APN.apn",
    "manei.model.mobile",
    function (console, kueQueue, mobileRule, gcm, apn, Mobile) {
        var self = {};

        var q = require('q');

        console.log('registrando manei.consumers.person');


        var apnPush = function (mobile, message) {
            var device_tokens = [];

            device_tokens.push(mobile.device_token_apn);

            return apn.push(device_tokens, message);

        };

        var gcmPush = function (mobile, message) {
            var device_tokens = [];

            device_tokens.push(mobile.device_token_gcm);

            return gcm.push(device_tokens, message);
        };

        var process = function (job, done) {

            console.log('Processando Person ...');

            var data = job.data;

            if (!data)
                return done('Fila Person sem dados para processar');

            var message = {};

            var personId = data.personId;

            message.msg = data.msg;
            message.title = data.title;
            message.data = data.data;

            var send = function (mobile) {
                return q.Promise(function (resolve, reject) {

                    if (!mobile.device_token_gcm && !mobile.device_token_apn) {
                        console.error('Esse mobile não tem mobile.device_token_gcm, mobile.device_token_apn');
                        return resolve('Esse mobile não tem mobile.device_token_gcm, mobile.device_token_apn')
                    }

                    if (mobile.device_token_gcm) {
                        gcmPush(mobile, message)
                                .then(function () {
                                    console.log('push to ' + mobile._id);
                                    return resolve('push to ' + mobile._id)
                                });
                    } else if (mobile.device_token_apn) {
                        apnPush(mobile, message)
                                .then(function () {
                                    console.log('push to ' + mobile._id);
                                    return resolve('push to ' + mobile._id)
                                });
                    }
                });
            };

            Mobile.find({
                person: personId
            })                    
                    .exec(function (err, mobiles) {
                        if (err)
                            return done(err);

                        var promises = [];
                        
                        var cacheKeyGCM = {};

                        for (var i in mobiles) {
                            var mobile = mobiles[i];
                            
                            if(!cacheKeyGCM[mobile.device_token_gcm])
                                promises.push(send(mobile));
                            
                            cacheKeyGCM[mobile.device_token_gcm] = true;
                        }

                        q.all(promises).spread(function () {
                            done();
                        });
                    });

        };

        kueQueue.consumer.process('person-push', process);

        return self;
    }
]);
