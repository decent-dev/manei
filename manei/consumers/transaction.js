inject.define("manei.consumers.transaction", [
    "kueQueue.kueQueue",
    "twilioSMS.api" + env,
    "manei.utils.util",
    "redisCache.api",
    "manei.config.config",
    "logApi.log",
    "manei.model.order",
    "manei.model.person",
    "manei.rule.transaction",
    "manei.rule.person",
    "manei.rule.order",
    function (kueQueue, sms, util, redisCache, config, console, Order, Person, ruleTransaction, personRule, orderRule) {
        var self = {};

        var q = require('q');

        var process = function (job, done) {

            console.log('Processando transaction ...');

            var data = job.data;

            var IDHash = data.IDHash;

            Order.findOrderByIDHash(IDHash)
                .then(function (order) {

                    if (!order) {
                        console.trace('OrderAuthorization não encontrado');
                        return done('OrderAuthorization não encontrado');
                    }

                    console.log('ID', IDHash, 'encontrado em transaction !!!');

                    var itens = order.itens;

                    if (itens.length == 0) return done('Pedido sem Itens');

                    var personIDs = itens.reduce(function (last, $this) {
                        if (!last['personFrom']) last['personFrom'] = $this.from;

                        if (!last['personTo']) last['personTo'] = $this.to;

                        return last;

                    }, {});

                    q.all([
                        personRule.resolvePersonById(personIDs.personFrom),
                        personRule.resolvePersonById(personIDs.personTo)
                    ]).spread(function (personFrom, personTo) {
                        ruleTransaction
                            .createTransactionByCpfCnpjPos(personFrom.cpfCnpj, personTo.cpfCnpj, order.total, order._id)
                            .then(function (data) {

                                order.status.push(orderRule.statusOrder.ORDERTRANSACTION);
                                order.save(function (err, order) {
                                    if (err) {
                                        console.trace(err);
                                        return done(undefined, 'Transação executada, mas não indicou a cobrança na Order: ' + err);
                                    }

                                    var value = data.transaction.amountReal.toFixed(2);

//                                    var job = kueQueue.create('sms', {
//                                        phoneNumber: data.phoneNumberFrom,
//                                        msg: ['Seu pedido foi aceito pelo estabelecimento', personTo.name].join(' ')
//                                    }).save();

                                    var job = kueQueue.create('person-push', {
                                        personId: personIDs.personFrom,
                                        msg: ['Seu pedido foi aceito pelo estabelecimento', personTo.name].join(' '),
                                        title : 'Pedido'
                                    }).save();

//                                    var job = kueQueue.create('sms', {
//                                        phoneNumber: data.phoneNumberFrom,
//                                        msg: ['Transação confirmada com sucesso', 'R$', value].join(' ')
//                                    }).save();

//                                    var job = kueQueue.create('person-push', {
//                                        personId: personIDs.personFrom,
//                                        msg: ['Transação confirmada com sucesso', 'R$', value].join(' '),
//                                        title : 'Transação'
//                                    }).save();

//                                    var job = kueQueue.create('sms', {
//                                        phoneNumber: data.phoneNumberTo,
//                                        msg: ['Transação confirmada com sucesso', 'R$', value].join(' ')
//                                    }).save();

//                                    var job = kueQueue.create('person-push', {
//                                        personId: personIDs.phoneNumberTo,
//                                        msg: ['Transação confirmada com sucesso', 'R$', value].join(' '),
//                                        title : 'Transação'
//                                    }).save();

                                    return done();
                                });
                            }).catch(function (err) {
                                console.trace(err);
                                return done(err);
                            });

                    }).catch(function (err) {
                        console.trace(err);
                        return done(err);
                    });
                })
                .catch(function (err) {
                    console.trace(err);
                    return done(err);
                });
        };

        kueQueue.consumer.process('transaction', process);

        return self;
    }
]);
