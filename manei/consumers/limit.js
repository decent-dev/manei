inject.define("manei.consumers.limit", [
    "logApi.log",
    "kueQueue.kueQueue",
    "manei.model.limit",
    function (console, kueQueue, Limit) {
        var self = {};

        var process = function (job, done) {
            var data = job.data;

            if (!data) return done('Fila Product sem dados para processar');

            var limit = new Limit(data);
            limit.save(function (err, d) {
                if (err) return done(err);

                return done(undefined, d);
            });
        };

        kueQueue.consumer.process('limit', process);

        return self;
    }
]);
