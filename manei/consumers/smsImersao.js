inject.define("manei.consumers.smsImersao", [
    "kueQueue.kueQueue",
    "twilioSMS.api" + env,
    "manei.utils.util" + env,
    "redisCache.api",
    "manei.config.config",
    "logApi.log",
    "kueQueue.kueQueue",
    "kueQueue.utils.util",
    function (kueQueue, sms, util, redisCache, config, console, kueQueue, logQueue) {
        var self = {};

        var process = function (job, done) {
            var data = job.data;

            var to = ['+', data.country, data.ddd, data.prefix, data.sulfix].join('');

            if(!data.country){
              to = data;
            }

            var confirmationCode = util.generateConfirmationCode();

            var msg = ['Código de confirmação', confirmationCode].join(', ');

            console.log('Enviando mensagem', msg, 'Para', to, '...');

            /**
             * Coloca o código de confirmação no Redis com TTL para posteriormente
             * validar
             */
            redisCache.setAndExpire(to, confirmationCode, config.ttlImersao)
                .then(function () {
                    var job = kueQueue.create('sms', {
                            phoneNumber: to,
                            msg: msg
                        }).save(function (err) {
                            if (err) {
                                console.trace(err);
                                return done(err);
                            }
                            console.log('Queued SMS');
                            return done();
                        });
                }).catch(function (err) {
                    console.trace(err);
                    return done(err);
                });

            logQueue.logQueue(job);
        };

        kueQueue.consumer.process('smsImersao', process);

        return self;
    }
]);
