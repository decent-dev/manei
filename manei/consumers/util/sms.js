inject.define("manei.consumers.util.sms", [
    "kueQueue.kueQueue",
    "twilioSMS.api" + env,
    "logApi.log",
    function (kueQueue, sms, console) {
        var self = {};

        var process = function (job, done) {

            console.log('Processando SMS ...');

            var data = job.data;

            if(!data) return done('Fila SMS sem dados para processar');

            var phoneNumber = data.phoneNumber;
            var msg = data.msg;

            if(!phoneNumber) return done('File SMS não foi passado o numero para envio');

            if(!msg) return done('File SMS não foi passado a mensagm');

            console.log('Enviando mensagem', msg, 'Para', phoneNumber, '...');

            /**
             * Envia para o Twilio o SMS
             */
            sms.sendMessage(phoneNumber, msg).then(function () {
                return done(undefined, ['SMS enviado com sucesso para', phoneNumber, '!!'].join(' '));
            }).catch(function (err) {
                console.trace(err);
                return done(err);
            });
        };

        kueQueue.consumer.process('sms', process);

        return self;
    }
]);
