inject.define("manei.consumers.agent", [
    "logApi.log",
    "kueQueue.kueQueue",
    "manei.model.agent",
    function (console, kueQueue, Agent) {
        var self = {};

        var MOUNTH = {
            'Jan': 1,
            'Feb': 2,
            'Mar': 3,
            'Apr': 4,
            'May': 5,
            'Jun': 6,
            'Jul': 7,
            'Aug': 8,
            'Sep': 9,
            'Oct': 10,
            'Nov': 11,
            'Dec': 12
        };

        var getDate = function (ts) {
            var regexTime = /^(\w{3}) (\w{3}) (\d{1,2}) (\d{2,4}) (\d{2}:\d{2}:\d{2}) (.*)$/;

            var time = new Date(ts).toString().replace(regexTime, '$5');
            var mounth = MOUNTH[new Date(ts).toString().replace(regexTime, '$2')];
            var day = new Date(ts).toString().replace(regexTime, '$3');
            return day + '/' + mounth + ' ' + time;
        };

        var log = function (body) {

            // var message = [];
            // message.push('[' + getDate(body.ts) + ']');
            // message.push('[W-' + body.method.toUpperCase() + ']');
            // message.push('[' + body.imei.split('').splice(0, 13).join('') + ']');
            // message.push('[' + body.stack + ']');
            // for (var m in body.args) message.push(body.args[m]);
            //
            // console[body.method].apply(this, message);
        };

        var process = function (job, done) {

            console.log('Processando Agent ...');

            var data = job.data;

            if(!data) return done('Fila Agent sem dados para processar');

            var agent = {};

            agent.thread = [data.auth, data.imei].join('#');
            agent.imei = data.imei;
            agent.auth = data.auth;

            agent.log = data;            

            new Agent(agent).save(function (err, agent) {
                if(err){
                  console.trace(err);
                  return done(err);
                }

                log(data);
                return done(undefined, 'Agente Salvo com sucesso');
            });


        };

        kueQueue.consumer.process('agent', process);

        return self;
    }
]);
