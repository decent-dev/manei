inject.define("manei.model.transaction", [
    "mongoDB.mongoDB",
    function (mongoDB) {

        var schema = mongoDB.Schema({
            personFrom: {
                type: mongoDB.Schema.ObjectId,
                ref: 'Person'
            },
            personTo: {
                type: mongoDB.Schema.ObjectId,
                ref: 'Person'
            },
            order: {
                type: mongoDB.Schema.ObjectId,
                ref: 'Order'
            },
            amountReal: Number,
            amountLTC: Number,
            transactionId: String,
            feeNetwork: Number,
            feeBlockIO: Number,
            network: String,
            feeEstimate: Number,
            dtcreate: {
                type: 'Date',
                default: Date.now
            }
        });

        schema.statics.agrupTotalByPersonFrom = function (person, cb) {
            return this.aggregate([{
                    $match: {
                        personFrom: person._id
                    }
                }, {
                    $group: {
                        _id: {
                            month : {
                                $month: "$dtcreate"
                            },
                            year : {
                                $year: "$dtcreate"
                            }
                            
                        },                        
                        ammount: {
                            $sum: '$amountReal'
                        }
                    }
                }], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(undefined, result);
                }
            });
        };

        schema.statics.agrupTotalByPersonTo = function (person, cb) {
            return this.aggregate([{
                    $match: {
                        personTo: person._id
                    }
                }, {
                    $group: {
                        _id: {
                            month : {
                                $month: "$dtcreate"
                            },
                            year : {
                                $year: "$dtcreate"
                            }
                            
                        },
                        ammount: {
                            $sum: '$amountReal'
                        }
                    }
                }], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(undefined, result);
                }
            });
        };

        var model = mongoDB.model('Transaction', schema);

        return model;
    }
]);
