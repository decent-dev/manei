inject.define("manei.model.product", [
    "mongoDB.mongoDB",
    function (mongoDB) {

        var schema = mongoDB.Schema({
            code: String,
            description : String,
            amount : Number,
            workflow : Number,
            person: {
                type: mongoDB.Schema.ObjectId,
                ref: 'Person'
            },
            dtcreate: {
                type: 'Date',
                default: Date.now
            },
            dtupdate: {
                type: 'Date',
                default: Date.now
            }
        });

        var model = mongoDB.model('Product', schema);

        return model;
    }
]);
