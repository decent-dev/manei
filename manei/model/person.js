inject.define("manei.model.person", [
    "mongoDB.mongoDB",
    function (mongoDB) {

        var schema = mongoDB.Schema({
            name: String,
            cpfCnpj: String,
            password: String,            
            dtcreate: {
                type: 'Date',
                default: Date.now
            },
            dtupdate: {
                type: 'Date',
                default: Date.now
            }
        });

        schema.statics.findByCpfCnpj = function (cpfCnpj) {
            return this.findOne({
                'cpfCnpj': cpfCnpj
            });
        };

        var model = mongoDB.model('Person', schema);

        return model;
    }
]);
