inject.define("manei.rule.mobile", [
    "manei.model.mobile",
    "manei.model.person",
    "manei.model.transaction",
    "blockio.api" + env,
    "manei.utils.util",
    "manei.config.config",
    "logApi.log",
    "kueQueue.kueQueue",
    "kueQueue.utils.util",
    function (Mobile, Person, Transaction, apiBlockIO, util, config, console, kueQueue, logQueue) {
        var self = {};

        var q = require('q');

        self.registerGCM = function (personId, device_token_gcm) {
            return q.Promise(function (resolve, reject, notify) {
                Mobile.update({
                    person: personId
                }, {
                    device_token_gcm: device_token_gcm
                }, function (err, mobile) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    if (!mobile) {
                        var msgtrace = ['GCM não foi atualizado'].join(' ');
                        console.trace(msgtrace);
                        return reject(msgtrace);
                    }                  
                    return resolve(mobile);
                });
            });
        };

        /**
         * Busca o Mobile a partir da pessoa Object
         */
        self.resolveMobileByPerson = function (person) {
            return q.Promise(function (resolve, reject, notify) {
                Mobile.findLastByPerson(person._id, function (err, mobile) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    if (!mobile) {
                        var msgtrace = ['Mobile não encontrado'].join(' ');
                        console.trace(msgtrace);
                        return reject(msgtrace);
                    }
                    return resolve(mobile);
                });
            });
        };

        self.resolveMobileByPersonId = function (personId) {
            return q.Promise(function (resolve, reject, notify) {

                if (!personId) return reject('Id da pessoa indefinido ao buscar Mobile: ' + personId);

                Mobile.findLastByPerson(personId, function (err, mobile) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!mobile) {
                        var msgtrace = ['Mobile não encontrado', personId].join(' ');
                        console.trace(msgtrace);
                        return reject(msgtrace);
                    }


                    return resolve(mobile);
                });
            });
        };



        /**
         * Retorna um Mobile a partir do label
         */
        self.resolveMobileByLabelPre = function (keyLabel) {
            return q.Promise(function (resolve, reject, notify) {
                Mobile.findByKeyLabelPre(keyLabel, function (err, mobile) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    if (!mobile) {
                        var msgtrace = ['Label', keyLabel, 'não encontrado'].join(' ');
                        console.trace(err);
                        return reject(msgtrace);
                    }
                    return resolve(mobile);
                });
            });
        };

        /**
         * Retorna um Mobile a partir do label
         */
        self.resolveMobileByLabelPos = function (keyLabel) {
            return q.Promise(function (resolve, reject, notify) {
                Mobile.findByKeyLabelPos(keyLabel, function (err, mobile) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    if (!mobile) {
                        var msgtrace = ['Label', keyLabel, 'não encontrado'].join(' ');
                        console.trace(err);
                        return reject(msgtrace);
                    }
                    return resolve(mobile);
                });
            });
        };

        self.createWalletForMobile = function (cpfCnpj, IMEI, phoneNumber, confirmationCode) {
            return q.Promise(function (resolve, reject, notify) {

                /**
                 * gera um hash do cpfCnpj para não divulgar o cpfCnpj do usuário
                 */
                var generateKeyLabelBlockIOPre = util.generateKeyLabelBlockIO(cpfCnpj, 'pre');
                var generateKeyLabelBlockIOPos = util.generateKeyLabelBlockIO(cpfCnpj, 'pos');

                console.log('Gerando KeyLabelBlockIOPre', generateKeyLabelBlockIOPre);
                console.log('Gerando KeyLabelBlockIOPos', generateKeyLabelBlockIOPos);

                /**
                 * deve existir um cadastro de pessoa para associar ao mobile em questão
                 */
                Person.findByCpfCnpj(cpfCnpj)
                    .then(function (person) {

                        if (!person) {
                            var msgtrace = ['CpfCnpj não encontrado'].join(' ');
                            console.trace(msgtrace);
                            return reject(msgtrace);
                        }

                        console.log('verifica se exite label no blockio se não ele cria');

                        /**
                         * verifica se exite label no blockio se não ele cria
                         */
                        q.all([
                                apiBlockIO.get_new_address_or_create(generateKeyLabelBlockIOPre),
                                apiBlockIO.get_new_address_or_create(generateKeyLabelBlockIOPos)
                            ]).spread(function (walletPre, walletPos) {

                                if (!walletPre) {
                                    var msgtrace = ['Label Pre não encontrado no Blockio'].join(' ');
                                    console.trace(msgtrace);
                                    return reject(msgtrace);
                                }

                                if (!walletPos) {
                                    var msgtrace = ['Label Pos não encontrado no Blockio'].join(' ');
                                    console.trace(msgtrace);
                                    return reject(msgtrace);
                                }

                                /**
                                 * verifica se existe Mobile cadastrado pelo IMEI
                                 */
                                Mobile.findByIMEI(IMEI, function (err, mobile) {
                                    if (err) {
                                        console.trace(err);
                                        return reject(err);
                                    }

                                    //TODO
                                    if (phoneNumber.indexOf('+') < 0) {
                                        phoneNumber = '+' + phoneNumber;
                                    }

                                    if (!mobile) {
                                        console.log('Mobile não existe. Criando...');
                                        new Mobile({
                                            phoneNumber: phoneNumber,
                                            IMEI: IMEI,
                                            person: person,
                                            confirmationCode: confirmationCode,
                                            keyLabel_pre: walletPre.data.label,
                                            keyLabel_pos: walletPos.data.label,
                                            address_pre: walletPre.data.address,
                                            address_pos: walletPos.data.address,
                                            dtcreate: new Date(),
                                            dtupdate: new Date()
                                        }).save(function (err, mobile) {
                                            if (err) {
                                                console.trace(err);
                                                return reject(err);
                                            }

                                            mobile.auth = [person._id, mobile._id].join('-');

                                            mobile.save(function (err, mobile) {
                                                if (err) {
                                                    console.trace(err);
                                                    return reject(err);
                                                }
                                                return resolve(mobile); // body...
                                            })

                                        });
                                    } else {

                                        console.log('Mobile ja existe. Atualizando...');
                                        /**
                                         * se o mobile ja existe, atualiza as informações.
                                         */
                                        mobile.auth = [person._id, mobile._id].join('-');
                                        mobile.phoneNumber = phoneNumber;
                                        mobile.confirmationCode = confirmationCode;
                                        mobile.keyLabel_pre = walletPre.data.label;
                                        mobile.keyLabel_pos = walletPos.data.label;
                                        mobile.address_pre = walletPre.data.address;
                                        mobile.address_pos = walletPos.data.address;
                                        mobile.person = person;
                                        mobile.dtupdate = new Date();
                                        mobile.confirmationCode = confirmationCode;
                                        mobile.phoneNumber = phoneNumber;
                                        mobile.save(function (err, mobile) {
                                            if (err) {
                                                console.trace(err);
                                                return reject(err);
                                            }
                                            return resolve(mobile);
                                        });
                                    }
                                });
                            })
                            .catch(function (err) {
                                console.trace(err);
                                return reject(err);
                            });
                    }).catch(function (err) {
                        console.trace(err);
                        return reject(err);
                    });

            });
        };

        var queue = function (queue, obj) {
            var job = kueQueue.create(queue, obj)
                .save(function (err) {
                    if (err) {
                        console.trace(err);
                        throw err;
                    }
                    console.log('Queued', queue);
                });
            logQueue.logQueue(job);
        };

        return self;
    }
]);
