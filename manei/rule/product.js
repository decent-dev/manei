inject.define("manei.rule.product", [
    "logApi.log",
    "manei.model.product",
    function (console, Product) {
        var self = {};

        var q = require('q');

        self.resolveProducts = function () {
            return q.Promise(function (resolve, reject, notify) {
              Product.find(function (err, products) {
                  if (err) {
                      console.trace(err);
                      return reject(err);
                  }

                  if(!products){
                    return reject('products indefinido');
                  }

                  return resolve(products);
              });

            });
        };

        self.resolvePaginationOrderByProducts = function (skip, limit, orderBy) {
            return q.Promise(function (resolve, reject, notify) {

              Product.find()
                .skip(skip)
                .limit(limit)
                .sort(orderBy)
                .exec(function (err, products) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if(!products){
                      return reject('products indefinido');
                    }

                    return resolve(products);
                });

            });
        };

        self.resolvePaginationProducts = function (skip, limit) {
            return q.Promise(function (resolve, reject, notify) {

              Product.find().skip(skip).limit(limit)
                .exec(function (err, products) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if(!products){
                      return reject('products indefinido');
                    }

                    return resolve(products);
                });

            });
        };

        self.resolveProductById = function (id) {
            return q.Promise(function (resolve, reject, notify) {

              if(!id) return reject('Id não foi passado');

              Product.findById(id, function (err, product) {
                  if (err) {
                      console.trace(err);
                      return reject(err);
                  }

                  if(!product){
                    return reject('Product não encontrada');
                  }

                  return resolve(product);
              });

            });
        };

        self.save = function (product) {
            return q.Promise(function (resolve, reject, notify) {

              if(!product) return reject('product não foi passado corretamente');

              new Product(product)
              .save(function (err, product) {
                  if (err) {
                      console.trace(err);
                      return reject(err);
                  }
                  return resolve(product);
              });

            });
        };


        return self;
    }
]);
