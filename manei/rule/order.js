inject.define("manei.rule.order", [
    "manei.model.order",
    "manei.model.person",
    "logApi.log",
    "manei.rule.person",
    "kueQueue.kueQueue",
    "kueQueue.utils.util",
    "manei.rule.mobile",
    "manei.model.limit",
    function (Order, Person, console, personRule, kueQueue, logQueue, mobileRule, Limit) {
        var self = {};

        var q = require('q');

        var statusOrder = {};

        statusOrder.ORDERREQUEST = {
            code: 0,
            label: 'Aberto pelo cliente'
        };

        statusOrder.ORDERACCEPT = {
            code: 1,
            label: 'Pedido foi aceito pelo estabelecimento'
        };

        statusOrder.ORDERTRANSACTION = {
            code: 2,
            label: 'Pedido Cobrado'
        };

        statusOrder.ORDERDONE = {
            code: 3,
            label: 'Pedido pronto para retirada'
        };

        statusOrder.ORDERREJECT = {
            code: 4,
            label: 'Pedido foi rejeitado'
        };

        self.statusOrder = statusOrder;

        self.resolveOrders = function () {
            return Order.find();
        };

        self.resolvePaginationOrderByPersonFrom = function (idUserTarget, skip, limit, orderBy) {
            return q.Promise(function (resolve, reject, notify) {

                Order.findOrderByIDPersonFrom(idUserTarget)
                        .skip(parseInt(skip))
                        .limit(parseInt(limit))
                        .sort(orderBy)
                        .exec(function (err, orders) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }

                            var promises = [];

                            for (var i in orders) {
                                var order = orders[i];
                                promises.push(self.resolveOrder(order));
                            }

                            q.all(promises).spread(function () {
                                return resolve(Array.prototype.slice.call(arguments));
                            }).catch(function (err) {
                                console.trace(err)
                                return reject(err);
                            });
                        });

            });
        };

        self.resolvePaginationOrderByPersonTo = function (idUserTarget, skip, limit, orderBy) {
            return q.Promise(function (resolve, reject, notify) {
                console.log(skip);
                Order.findOrderByIDPersonTo(idUserTarget)
                        .sort(orderBy)
                        .skip(parseInt(skip))
                        .limit(parseInt(limit))
                        .exec(function (err, orders) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }

                            var promises = [];

                            for (var i in orders) {
                                var order = orders[i];
                                promises.push(self.resolveOrder(order));
                            }

                            q.all(promises).spread(function () {
                                return resolve(Array.prototype.slice.call(arguments));
                            }).catch(function (err) {
                                console.trace(err)
                                return reject(err);
                            });
                        });

            });
        };

        self.resolveOrderByIDHash = function (IDHash) {
            return q.Promise(function (resolve, reject, notify) {

                if (!IDHash)
                    return reject('IDHash não foi passado');

                Order.findOrderByIDHash(IDHash)
                        .then(function (order) {

                            if (!order) {
                                return reject('Order não encontrada');
                            }

                            self.resolveOrder(order)
                                    .then(function (order) {
                                        return resolve(order);
                                    }).catch(function (err) {
                                console.trace(err)
                                return reject(err);
                            });
                        }).catch(function (err) {
                    console.trace(err)
                    return reject(err);
                });

            });
        };

        self.resolveOrder = function (order) {
            return q.Promise(function (resolve, reject, notify) {

                var promises = [];

                promises.push(personRule.resolvePersonNameById(order.personFrom));
                promises.push(personRule.resolvePersonNameById(order.personTo));

                q.all(promises).spread(function (personFromName, personToName) {
                    return resolve({
                        _id: order._id,
                        status: order.status,
                        obs: order.obs,
                        personFrom: personFromName,
                        personTo: personToName,
                        dtcreate: order.dtcreate,
                        itens: order.itens,
                        total: order.total,
                        IDHash: order.IDHash
                    });
                }).catch(function (err) {
                    console.trace(err);
                    return reject(err);
                });
            });
        };

        self.reject = function (order) {
            return q.Promise(function (resolve, reject, notify) {
                if (!order || !order.IDHash)
                    return reject('Id da Order não definido')
                console.log('rejectOrder', order.IDHash);
                Order.findOrderByIDHash(order.IDHash)
                        .then(function (order) {

                            new Limit({
                                person: order.personFrom,
                                ammount: (order.total),
                                status: statusOrder.ORDERREJECT
                            }).save(function (err, limit) {
                                if (err) {
                                    console.trace(err);
                                    return reject(err);
                                }

                                if (!order) {
                                    return reject('Pedido não encontrado');
                                } else {

                                    for (var i in order.status) {
                                        var status = order.status[i];
                                        if (status.code == statusOrder.ORDERREJECT.code) {
                                            console.log('Pedido ja foi rejeitado anteriormente');
                                            return resolve(order);
                                        }
                                        
                                        if (status.code == statusOrder.ORDERACCEPT.code) {
                                            console.log('Pedido ja foi aceito, não pode rejeitar esse pedido');
                                            return resolve(order);
                                        }
                                    }

                                    console.log('ID', order.IDHash, 'encontrado !!!');
                                    order.status.push(statusOrder.ORDERREJECT);
                                    order.save(function (err, order) {
                                        if (err) {
                                            console.trace(err);
                                            return reject(err);
                                        }

                                        mobileRule
                                                .resolveMobileByPersonId(order.personFrom)
                                                .then(function (mobile) {

//                                                    queue('sms', {
//                                                        phoneNumber: mobile.phoneNumber,
//                                                        msg: 'Sua Ordem foi rejeitada'
//                                                    });

                                                    queue('person-push', {
                                                        personId: order.personFrom,
                                                        msg: 'Sua Ordem foi rejeitada',
                                                        title: 'Pedido'
                                                    });

                                                    return resolve(order);
                                                });
                                    });
                                }

                            });

                        }).catch(function (err) {
                    console.trace(err);
                    return reject(err);
                });
            });
        };
        self.save = function (order) {
            return q.Promise(function (resolve, reject, notify) {

                if (!order)
                    return reject('Order não foi passado corretamente');

                var itens = order.itens;

                if (itens.length == 0)
                    return reject('Pedido sem Itens');

                var personIDs = itens.reduce(function (last, $this) {
                    if (!last['personFrom'])
                        last['personFrom'] = $this.from;

                    if (!last['personTo'])
                        last['personTo'] = $this.to;

                    return last;

                }, {});

                Order.findOrderByIDHash(order.IDHash)
                        .then(function (_order) {
                            q.all([
                                personRule.resolvePersonById(personIDs.personFrom),
                                personRule.resolvePersonById(personIDs.personTo)
                            ]).spread(function (personFrom, personTo) {
                                new Limit({
                                    person: personFrom,
                                    ammount: (order.total * -1),
                                    status: statusOrder.ORDERREQUEST
                                }).save(function (err, limit) {
                                    if (err) {
                                        console.trace(err);
                                        return reject(err);
                                    }

                                    if (!_order) {
                                        var orderPersist = new Order(order);
                                        console.warn('ID', order.IDHash, 'não encontrado');
                                        orderPersist.status.push(statusOrder.ORDERREQUEST);
                                        orderPersist.personFrom = personFrom;
                                        orderPersist.personTo = personTo;

                                        orderPersist
                                                .save(function (err, order) {
                                                    if (err) {
                                                        console.trace(err);
                                                        return reject(err);
                                                    }
                                                    console.log('Order criada');

                                                    mobileRule
                                                            .resolveMobileByPersonId(order.personTo)
                                                            .then(function (mobile) {

//                                                                queue('sms', {
//                                                                    phoneNumber: mobile.phoneNumber,
//                                                                    msg: 'Existe um novo pedido de ' + personFrom.name
//                                                                });

                                                                queue('person-push', {
                                                                    personId: personIDs.personTo,
                                                                    msg: 'Existe um novo pedido',
                                                                    title: 'Pedido - ' + personFrom.name,
                                                                    data : order
                                                                });

                                                                return resolve(order);
                                                            });
                                                });
                                    } else {
                                        for (var i in order.status) {
                                            var status = order.status[i];
                                            console.log(status.label);
                                        }

                                        return resolve(_order);
                                    }
                                });

                            }).catch(function (err) {
                                console.trace(err);
                                return reject(err);
                            });
                        }).catch(function (err) {
                    console.trace(err);
                    return reject(err);
                });

            });
        };

        self.done = function (order) {
            return q.Promise(function (resolve, reject, notify) {
                Order.findOrderByIDHash(order.IDHash)
                        .then(function (order) {
                            for (var i in order.status) {
                                var status = order.status[i];
                                if (status.code == statusOrder.ORDERDONE.code) {
                                    console.log('Pedido ja foi Terminado anteriormente');
                                    return resolve(order);
                                }
                            }

                            console.log('ID', order.IDHash, 'encontrado !!!');
                            order.status.push(statusOrder.ORDERDONE);
                            order.save(function (err, order) {
                                if (err) {
                                    console.trace(err);
                                    return reject(err);
                                }

                                mobileRule
                                        .resolveMobileByPersonId(order.personFrom)
                                        .then(function (mobile) {

//                                            queue('sms', {
//                                                phoneNumber: mobile.phoneNumber,
//                                                msg: 'Seu Pedido esta pronto. Favor retirar'
//                                            });

                                            queue('person-push', {
                                                personId: order.personFrom,
                                                msg: 'Seu Pedido esta pronto. Favor retirar',
                                                title: 'Pedido'
                                            });

                                            return resolve(order);

                                        }).catch(function (err) {
                                    console.trace(err);
                                    return reject(err);
                                });

                            });
                        }).catch(function (err) {
                    console.trace(err);
                    return reject(err);
                });
            });
        };

        self.accept = function (order) {
            return q.Promise(function (resolve, reject, notify) {

                if (!order)
                    return reject('Order não foi passado corretamente');

                Order.findOrderByIDHash(order.IDHash)
                        .then(function (_order) {

                            if (!_order) {
                                self.save(order)
                                        .then(function (order) {
                                            order.status.push(statusOrder.ORDERACCEPT);
                                            order.save(function (err, order) {
                                                if (err) {
                                                    console.trace(err);
                                                    return reject(err);
                                                }
                                                console.log('Order criada');

                                                queue('transaction', order);

                                                return resolve(order);
                                            });
                                        })
                                        .catch(function (err) {
                                            console.trace(err);
                                            return reject(err);
                                        });

                            } else {

                                for (var i in _order.status) {
                                    var status = _order.status[i];
                                    if (status.code == statusOrder.ORDERACCEPT.code) {
                                        console.log('Order ja fora aceito anteriormente');
                                        return resolve(order);
                                    }
                                }

                                _order.status.push(statusOrder.ORDERACCEPT);

                                _order.save(function (err, order) {
                                    if (err) {
                                        console.trace(err);
                                        return reject(err);
                                    }
                                    console.log('Order criada');

                                    queue('transaction', _order);

                                    return resolve(order);
                                });
                            }
                        }).catch(function (err) {
                    console.trace(err);
                    return reject(err);
                });
            });
        };

        var queue = function (queue, obj) {
            var job = kueQueue.create(queue, obj)
                    .save(function (err) {
                        if (err) {
                            console.trace(err);
                            throw err;
                        }
                        console.log('Queued', queue);
                    });
            logQueue.logQueue(job);
        };

        return self;
    }
]);
