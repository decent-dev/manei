inject.define("manei.rule.person", [
    "manei.model.mobile",
    "manei.model.person",
    "manei.model.order",
    "manei.model.transaction",
    "blockio.api" + env,
    "manei.utils.util",
    "manei.config.config",
    "logApi.log",
    "manei.rule.mobile",
    "manei.model.limit",    
    function (Mobile, Person, Order, Transaction, apiBlockIO, util, config, console, mobileRule, Limit) {
        var self = {};

        var q = require('q');
        
        self.resumoTransactionReceitaPos = function (personId) {
            return q.Promise(function (resolve, reject, notify) {
                self.resolvePersonById(personId)
                    .then(function (person) {

                        Transaction.agrupTotalByPersonFrom(person, function (err, resumo) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }

                            if (!resumo) {
                                return reject('resumo indefinido');
                            }

                            return resolve(resumo);
                        });
                    });
            });
        };

        /**
         * Retorna o saldo da pessoa
         * a pessoa é um Object
         */
        self.resolveBalancePreByPerson = function (person) {
            return q.resolve(person)
                .then(mobileRule.resolveMobileByPerson)
                .then(function (mobile) {
                    return q.Promise(function (resolve, reject, notify) {
                        apiBlockIO.get_label_balance(mobile.keyLabel_pre)
                            .then(function (balance) {
                                console.log('Converte o valor LTC em Real baseado no fator Manei', config.conversionFactor);

                                /**
                                 * Converte o valor LTC em Real baseado no fator Manei
                                 */
                                var amountReal = balance.data.available_balance / config.conversionFactor;
                                return resolve(amountReal);
                            })
                            .catch(function (err) {
                                console.trace(err);
                                return reject(err);
                            });
                    });
                });

        };
        
        self.resolvePersonById = function (id) {
            return q.Promise(function (resolve, reject, notify) {
                Person.findById(id, function (err, person) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!person) {
                        return reject('Pessoa não encontrada');
                    }

                    return resolve(person);
                });
            });
        };

        /**
         * Retorna uma Pessoa a partir do Address
         */
        self.resolvePersonByAddress = function (address) {
            return q.Promise(function (resolve, reject, notify) {
                Mobile.findByKeyAddress(address, function (err, mobile) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    if (!mobile) {
                        var msgtrace = ['Address', address, 'não encontrado'].join(' ');
                        console.trace(msgtrace);
                        return reject(msgtrace);
                    }

                    Person.findById(mobile.person, function (err, person) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!person) {
                            var msgtrace = ['Pessoal não encontrada com address', address].join(' ');
                            console.trace(msgtrace);
                            return reject(msgtrace);
                        }

                        return resolve(person);
                    });
                });
            });
        };

        self.resolvePersonNameById = function (idUser) {
            return q.Promise(function (resolve, reject, notify) {

                if (!idUser) return resolve('Não encontrado');

                Person.findById(idUser, function (err, person) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    return resolve(person.name);
                });
            });
        };

        self.getMobileMasterByCpfCnpj = function (cpfCnpj) {
            return q.Promise(function (resolve, reject, notify) {
                Person.findByCpfCnpj(cpfCnpj)
                    .then(function (person) {
                        if (!person) {
                            return reject('Pessoa não encontrada');
                        }
                        Mobile.findLastByPerson(person._id, function (err, mobile) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }

                            if (!mobile) {
                                var msgtrace = ['Mobile não encontrada'].join(' ');
                                console.log(msgtrace);
                                return reject(msgtrace);
                            }

                            if (mobile) {
                                return resolve(mobile);
                            }
                        });
                    }).catch(function (err) {
                        console.trace(err);
                        return reject(err);
                    });
            });
        };

        self.getUserName = function (id) {
            return q.Promise(function (resolve, reject, notify) {
                Person.findById(id, function (err, person) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!person) {
                        var msgtrace = ['Pessoa não encontrada'].join(' ');
                        console.log(msgtrace);
                        return reject(msgtrace);
                    }

                    return resolve(person.name);
                });
            });
        };

        /**
         * Cria a pessoa na base de dados
         */
        self.createPerson = function (name, cpfCnpj, password) {

            console.log('Gerando MD5 para password');
            password = util.md5(password);

            return q.Promise(function (resolve, reject, notify) {

                if (!name) {
                    var msgtrace = ['Nome não foi passado'].join(' ');
                    console.trace(msgtrace);
                    return reject(msgtrace);
                }

                if (!cpfCnpj) {
                    var msgtrace = ['CPF ou CNPJ não foi passado'].join(' ');
                    console.trace(msgtrace);
                    return reject(msgtrace);
                }

                if (!password) {
                    var msgtrace = ['Senha não foi passado'].join(' ');
                    console.trace(msgtrace);
                    return reject(msgtrace);
                }

                Person.findByCpfCnpj(cpfCnpj).then(function (person) {

                    if (person) {
                        console.log('Pessoa já existe, Atualizando');
                        person.name = name;
                        person.password = password;
                        person.save(function (err, p) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }

                            return resolve(p);
                        });
                    } else {
                        console.log('Pessoa não existe, Criandp');
                        new Person({
                            name: name,
                            cpfCnpj: cpfCnpj,
                            password: password,
                            dtcreate: new Date(),
                            dtupdate: new Date()
                        }).save(function (err, person) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }
                            return resolve(person);
                        });
                    }
                }).catch(function (err) {
                    console.trace(err);
                    return reject(err);
                });;
            });
        };

        /**
         * Retorna uma Pessoa a partir do label
         */
        self.resolvePersonByLabel = function (label) {
            return q.Promise(function (resolve, reject, notify) {
                Mobile.findByKeyLabelPre(label, function (err, mobile) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    if (!mobile) {
                        var msgtrace = ['Label não encontrado'].join(' ');
                        console.trace(msgtrace);
                        return reject(msgtrace);
                    }

                    Person.findById(mobile.person, function (err, person) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!person) {
                            var msgtrace = ['Pessoal não encontrada para o Label Mobile', label].join(' ');
                            console.trace(msgtrace);
                            return reject(msgtrace);
                        }

                        return resolve(person);
                    });
                });
            });
        };

        /**
         * Retorna uma Pessoa a partir do label
         */
        self.resolvePersonByLabelPos = function (label) {
            return q.Promise(function (resolve, reject, notify) {
                Mobile.findByKeyLabelPos(label, function (err, mobile) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    if (!mobile) {
                        var msgtrace = ['Label não encontrado'].join(' ');
                        console.trace(msgtrace);
                        return reject(msgtrace);
                    }

                    Person.findById(mobile.person, function (err, person) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!person) {
                            var msgtrace = ['Pessoal não encontrada para o Label Mobile', label].join(' ');
                            console.trace(msgtrace);
                            return reject(msgtrace);
                        }

                        return resolve(person);
                    });
                });
            });
        };

        return self;
    }
]);
