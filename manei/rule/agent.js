inject.define("manei.rule.agent", [
    "logApi.log",
    "manei.model.agent",
    function (console, Agent) {
        var self = {};

        var q = require('q');

        self.resolveAgents = function () {
            return q.Promise(function (resolve, reject, notify) {
                Agent.find(function (err, agents) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!agents) {
                        return reject('agents indefinido');
                    }

                    return resolve(agents);
                });

            });
        };

        self.resolvePaginationOrderByAgents = function (skip, limit, orderBy) {
            return q.Promise(function (resolve, reject, notify) {

                Agent.find()
                    .skip(skip)
                    .limit(limit)
                    .sort(orderBy)
                    .exec(function (err, agents) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!agents) {
                            return reject('agents indefinido');
                        }

                        return resolve(agents);
                    });

            });
        };

        self.resolvePaginationAgents = function (skip, limit) {
            return q.Promise(function (resolve, reject, notify) {

                Agent.find().skip(skip).limit(limit)
                    .exec(function (err, agents) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!agents) {
                            return reject('agents indefinido');
                        }

                        return resolve(agents);
                    });

            });
        };

        self.resolveAgentById = function (id) {
            return q.Promise(function (resolve, reject, notify) {

                if (!id) return reject('Id não foi passado');

                Agent.findById(id, function (err, agent) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!agent) {
                        return reject('Agent não encontrada');
                    }

                    return resolve(agent);
                });

            });
        };

        self.save = function (agent) {
            return q.Promise(function (resolve, reject, notify) {

                if (!agent) return reject('agent não foi passado corretamente');

                new Agent(agent)
                    .save(function (err, agent) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }
                        return resolve(agent);
                    });

            });
        };

        return self;
    }
]);
