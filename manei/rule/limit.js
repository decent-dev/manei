inject.define("manei.rule.limit", [
    "logApi.log",
    "manei.model.limit",
    "manei.rule.person",
    function (console, Limit, personRule) {
        var self = {};

        var q = require('q');

        self.resolveLimitByPerson = function (personId) {
            return q.Promise(function (resolve, reject, notify) {
                personRule.resolvePersonById(personId)
                    .then(function (person) {

                        Limit.sumLimitByPerson(person, function (err, limits) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }

                            if (!limits) {
                                return reject('limits indefinido');
                            }

                            return resolve(limits[0]);
                        });
                    });
            });
        };

        self.resolveSaldoPagarByPerson = function (personId) {
            return q.Promise(function (resolve, reject, notify) {
                personRule.resolvePersonById(personId)
                    .then(function (person) {

                        Limit.sumSaldoPagarByPerson(person, function (err, limits) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }

                            if (!limits) {
                                return reject('limits indefinido');
                            }

                            return resolve(limits[0]);
                        });
                    });
            });
        };

        self.resolveLimits = function () {
            return q.Promise(function (resolve, reject, notify) {
                Limit.find(function (err, limits) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!limits) {
                        return reject('limits indefinido');
                    }

                    return resolve(limits);
                });

            });
        };

        self.resolvePaginationOrderByLimits = function (skip, limit, orderBy) {
            return q.Promise(function (resolve, reject, notify) {

                Limit.find()
                    .skip(skip)
                    .limit(limit)
                    .sort(orderBy)
                    .exec(function (err, limits) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!limits) {
                            return reject('limits indefinido');
                        }

                        return resolve(limits);
                    });

            });
        };

        self.resolvePaginationLimits = function (skip, limit) {
            return q.Promise(function (resolve, reject, notify) {

                Limit.find().skip(skip).limit(limit)
                    .exec(function (err, limits) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!limits) {
                            return reject('limits indefinido');
                        }

                        return resolve(limits);
                    });

            });
        };

        self.resolveLimitById = function (id) {
            return q.Promise(function (resolve, reject, notify) {

                if (!id) return reject('Id não foi passado');

                Limit.findById(id, function (err, agent) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!agent) {
                        return reject('Limit não encontrada');
                    }

                    return resolve(agent);
                });

            });
        };

        self.save = function (agent) {
            return q.Promise(function (resolve, reject, notify) {

                if (!agent) return reject('agent não foi passado corretamente');

                new Limit(agent)
                    .save(function (err, agent) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }
                        return resolve(agent);
                    });

            });
        };

        return self;
    }
]);
