inject.define("redisCache.api", [
	"redisCache.redisCache",
    function(redisCache) {
        var self = {};
		
        var q = require('q');

        self.set = function(key, value){
        	return q.Promise(function(resolve, reject, notify){
        		redisCache.set(key, value, function(err, reply){
        			if(err){
						console.trace(err);
        				return reject(err);
        			}
        			resolve(reply);
        		});
        	});
        };

        self.get = function(key){
        	return q.Promise(function(resolve, reject, notify){
        		redisCache.get(key, function(err, reply){
        			if(err){
						console.trace(err);
        				return reject(err);
        			}
        			resolve(reply);
        		});
        	});
        };

        self.expire = function(key, ttl){
            return q.Promise(function(resolve, reject, notify){
                redisCache.expire(key, ttl, function(err, reply){
                    if(err){
						console.trace(err);
                        return reject(err);
                    }
                    resolve(reply);
                });
            });
        };

        self.setAndExpire = function(key, value, ttl){
            return q.Promise(function(resolve, reject, notify){
                self.set(key, value)
                .then(function(reply){
                    self.expire(key, ttl)
                    .then(function(reply){
                        resolve(reply);
                    })
                    .catch(function(err){
						console.trace(err);
                        return reject(err);
                    });
                })
                .catch(function(err){
					console.trace(err);
                    return reject(err);
                });    
            });
        };

        return self;
    }
]);
