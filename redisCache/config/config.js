inject.define("redisCache.config.config", [	
    function() {
        var self = {};
        
        self.port = process.env.PORTREDIS || 6379;
	    self.host = process.env.HOSTREDIS || 'localhost';	    

        return self;
    }
]);
